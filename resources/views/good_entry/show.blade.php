@extends('layouts.app')
@section('htmlheader_title')
{{ __('good_entry.delivery_order') }}
@stop

@section('contentheader_buttons')
    <a id="btn_print" class="btn btn-default" >{{ __('generic.print') }}</a>
    <a href="{{ PreviousRoute::getNamedRoute('session.index.back')}}" class="btn btn-default" >{{ __('good_entry.cancel') }} </a>
@endsection

@section('main-content')
<br>
<section id="formWrapper" class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i>
            {{ setting('site.title') }}
            <small class="pull-right">{{ $good_entry->date->isoFormat('LL') }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-10 invoice-col">
          <b>{{ __('good_entry.good_entry') }} #{{ $good_entry->id }}</b><br>
          <br>
          <b>{{ __('good_entry.doc_num') }}:</b> {{ $good_entry->doc_serie }} {{ $good_entry->doc_num }}<br>
          <b>{{ __('good_entry.doc_date') }}:</b> {{ $good_entry->doc_date_locale }} <br>
          <b>{{ __('good_entry.business_partner_id') }}:</b> {{ $good_entry->business_partner_id }}
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Cantidad</th>
                    <th>Material</th>
                    <th>Medida</th>
                    <th>Precio</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach($detailGoodEntry as $key=>$item)
                    <tr class="detail_good_entry" data-id="{{ $item->id }}"> 
                        <td>{{ $loop->iteration }}</td>
                        <td data-cell='DTA' data-format="0,0[.]00" >{{ $item->quantity }}</td>
                        <td>{{ $item->material }}</td>
                        <td>{{ $item->measure }}</td>
                        <td data-cell='DTB' data-format="0[.]00" >{{ $item->price }}</td>
                        <td data-cell="{{ 'DTC'.$loop->iteration}}" data-format="0[.]00" >{{ $item->subtotal }}</td>
                    </tr>
                @endforeach
                </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">{{ __('good_entry.status_id') }}: {{ $good_entry->status_description($good_entry->status_id) }}</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>{{ __('good_entry.total') }}:</th>
                <td>{{ $good_entry->total }}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
        </div>
      </div>
    </section>

@endsection

@section('scripts')
<script src="{{ asset('/plugins/jquery-calx/numeral.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/jquery-calx/jquery-calx.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    function printElem(divId) {
        var printContents = document.getElementById(divId).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
        return true;
    }

    $(document).ready(function() {
        $('#formWrapper').calx({
            readonly : true
        });

        $('section.content').attr('id','page_container');

        $(document).on("click","#btn_print", function(e)  {
            printElem('page_container');
        });

    });
</script>
@stop