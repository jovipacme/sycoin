<a href="{{ route('stocks.edit', ['id'=> $item->id] ) }}" class="btn btn-primary btn-xs edit_stock"
data-toggle='modals' data-remote='false' data-target = '#stockModal' data-mode='edit' >
    {{ __('generic.update') }}
</a> 
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['stocks', $item->id ],
    'style' => 'display:inline'
]) !!}
    {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
{!! Form::close() !!}