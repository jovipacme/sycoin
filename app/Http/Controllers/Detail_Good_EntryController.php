<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\PreviousRoute;
use App\Detail_Good_Entry;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class Detail_Good_EntryController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'good_entry_id' => 'required',
        'material_id' => 'required',
        'measure_id' => 'required',
        'qunatity' => 'numeric',
        'price' => 'numeric',
        'subtotal'=>'numeric'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $detailGoodEntries = Detail_Good_Entry::all();

        PreviousRoute::setNamedRoute('session.index.back');
        return view('detail_good_entries.index', compact('detailGoodEntries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $materialList = array();
        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        return view('detail_good_entries.create',compact('materialList','measureList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        } else{
            toast()->clear();
        }

        $data = Detail_Good_Entry::create($request->all());

        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Detail_Good_Entry::select("detail_good_entries.id","detail_good_entries.good_entry_id",
            "materials.name AS material","measures.name AS measure","detail_good_entries.quantity","detail_good_entries.price",
            "detail_good_entries.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_good_entries.material_id')
            ->join('measures', 'measures.id', '=', 'detail_good_entries.measure_id')
            ->where('detail_good_entries.id','=',$data->id)
            ->first();

            return response()->json($detail);
        } else {
            toast()->success('message', 'Detail_Good_Entry added!');
    
            return redirect('detail_delivery_order');
        }        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        $detailGoodEntry = Detail_Good_Entry::findOrFail($id);

        if( $request->ajax() ){
            //return response()->json($detailGoodEntry);
            return view('detail_good_entries.show_fields', compact('detailGoodEntry'));
        } else {
            return view('detail_good_entries.show', compact('detailGoodEntry'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        if (empty($id)==false) {
            
            $detailGoodEntry = Detail_Good_Entry::select("detail_good_entries.id","detail_good_entries.good_entry_id",
            "detail_good_entries.material_id","materials.name AS material","measures.name AS measure","detail_good_entries.quantity","detail_good_entries.price",
            "detail_good_entries.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_good_entries.material_id')
            ->join('measures', 'measures.id', '=', 'detail_good_entries.measure_id')
            ->where('detail_good_entries.id','=',$id)->firstOrFail();

            $materialList = \App\Material::select("id","code","name","description")
                ->where('id','=',$detailGoodEntry['material_id'])
                ->pluck('name', 'id');

            $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        } else {
            $detailGoodEntry = Detail_Good_Entry::findOrFail($id);
            $materialList = array();
            $measureList = array();
        }
        

        return view('detail_good_entries.edit', compact('detailGoodEntry','materialList','measureList'));

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        } else{
            toast()->clear();
        }

        $detailGoodEntries = Detail_Good_Entry::findOrFail($id);
        $detailGoodEntries->update($request->all());

        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Detail_Good_Entry::select("detail_good_entries.id","detail_good_entries.good_entry_id",
            "materials.description AS material","measures.name AS measure","detail_good_entries.quantity","detail_good_entries.price",
            "detail_good_entries.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_good_entries.material_id')
            ->join('measures', 'measures.id', '=', 'detail_good_entries.measure_id')
            ->where('detail_good_entries.id','=',$id)
            ->first();

            toast()->success('message', 'Detail_Good_Entry updated!');
            return response()->json($detail);
        } else {
            
            return redirect('detail_delivery_order');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy(Request $request,$id)
    {
        $detailGoodEntries = Detail_Good_Entry::findOrFail($id);

        $detailGoodEntries->delete();

        //Si es una peticion ajax
        if( $request->ajax() ){
            return response()->json($detailGoodEntries);
        } else {
        Session::flash('message', 'Detail_Good_Entry deleted!');
        Session::flash('status', 'success');
        }
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataMaterials(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = DB::table("materials")
                    ->select("id","code","name","description")
                    ->Where('name','LIKE',"%$search%")
                    ->orWhere('description','LIKE',"%$search%")
                    ->where('deleted_at','=',Null)
                    ->get();
        }
        return response()->json($data);
    } 

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataMeasures(Request $request)
    {
        $data = [];

        if($request->has('material_id')){
            $search = $request->material_id;
            $data = DB::table("material_measures")
                    ->select("id","name","unit")
                    ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
                    ->where('material_measures.material_id','=',$request->material_id)
                    ->where('material_measures.deleted_at','=',Null)
                    ->get();
        }
        return response()->json($data);
    } 

}
