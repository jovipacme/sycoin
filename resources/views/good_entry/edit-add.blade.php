@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.add_new') }} {{ __('good_entry.good_entry') }}
@stop

@section('main-content')

<h1>{{ __('generic.add') }} {{ __('good_entry.good_entry') }}</h1>
<hr/>
<div id="formWrapper" class=""> <!-- main wrapper !--> 
    <form class="form-edit-add" role="form" id="frmGoodEntry" class="form-horizontal"
            action="{{ (isset($good_entry->id)) ? route('good_entry.update', $good_entry->id) : route('good_entry.store') }}"
            method="POST" enctype="multipart/form-data" autocomplete="off">
        <!-- PUT Method if we are editing -->
        @if(isset($good_entry->id))
            {{ method_field("PUT") }}
        @endif
        {{ csrf_field() }}

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Encabezado</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>        
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group {{ $errors->has('business_partner_id') ? 'has-error' : ''}}">
                            {!! Form::label('business_partner_id', __('good_entry.business_partner_id'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @php $business_partner_id = (isset($good_entry->business_partner_id)) ? $good_entry->business_partner_id : null  @endphp
                                {!! Form::select('business_partner_id', $customerList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('business_partner_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="col-sm-3">
                            <a href="{{ URL::route('good_entry.addProvider') }}" data-remote="false" data-toggle="modal" data-target="#customerModal" class="btn btn-success">
                                <i class="glyphicon glyphicon-plus"></i> {{__('generic.add')}}
                            </a>
                            </div>                   
                        </div>
                    </div>            
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('doc_date') ? 'has-error' : ''}}">
                            {!! Form::label('doc_date', __('good_entry.doc_date'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $doc_date = (isset($good_entry->doc_date)) ? $good_entry->doc_date : null  @endphp
                                {!! Form::date('doc_date', $doc_date, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_date', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>                
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('partner_document_id') ? 'has-error' : ''}}">
                            {!! Form::label('partner_document_id', __('good_entry.partner_document_id'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $partner_document_id = (isset($good_entry->partner_document_id)) ? $good_entry->partner_document_id : null  @endphp
                                {!! Form::select('partner_document_id', $businessPartnerDocumentList, $partner_document_id, ['class' => 'form-control']) !!}
                                {!! $errors->first('partner_document_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group {{ $errors->has('doc_serie') ? 'has-error' : ''}}">
                            {!! Form::label('doc_serie', __('good_entry.doc_serie'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-8">
                                @php $doc_serie = (isset($good_entry->doc_serie)) ? $good_entry->doc_serie : null  @endphp
                                {!! Form::text('doc_serie', $doc_serie, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_serie', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('doc_num') ? 'has-error' : ''}}">
                            {!! Form::label('doc_num', __('good_entry.doc_num'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-8">
                                @php $doc_num = (isset($good_entry->doc_num)) ? $good_entry->doc_num : null  @endphp
                                {!! Form::text('doc_num', $doc_num, ['class' => 'form-control']) !!}
                                {!! $errors->first('doc_num', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
                            {!! Form::label('total', __('good_entry.total'), ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-9">
                                @php $total = (isset($good_entry->total)) ? $good_entry->total : null  @endphp
                                {!! Form::text('total', $total, ['class' => 'form-control text-right','data-format'=>"0.00",
                                    'readonly'=>'','data-cell'=>"ST0",'data-formula'=>'']) !!}
                                {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                @if (!$good_entry)
                <div class="row">            
                    <div class="col-sm-8">&nbsp;</div>
                    <div class="col-sm-3">
                        <div class="form-inline">
                        {!! Form::submit( __('good_entry.create'), ['class' => 'btn btn-info form-control']) !!}
                        {{ link_to(PreviousRoute::getNamedRoute('session.index.back'), __('good_entry.cancel'), ['class' => 'btn btn-default form-control']) }}
                        </div>
                    </div>            
                </div>
                @else
                <div class="row">
                    <div class="col-sm-8">&nbsp;</div>
                    <div class="col-sm-4">
                        <div class="form-inline">
                        {!! Form::submit( __('generic.save'), ['class' => 'btn btn-info form-control']) !!}
                        @if ( $good_entry->status_id != config('constants.DocumentStatus.closed') )
                            {{ link_to( route('good_entry.process', ['id'=>$good_entry->id,'business_partner_id'=>$good_entry->business_partner_id]), 
                                __('generic.register'), ['id' => 'process','class' => 'btn btn-success form-control']) }}
                        @endif
                        {{ link_to(PreviousRoute::getNamedRoute('session.index.back'), __('good_entry.cancel'), ['class' => 'btn btn-default form-control']) }}
                        </div>
                    </div>
                </div>                
                @endif
            </div>
        </div>
    </form>
        @if ($good_entry)
        <div id="boxDetailDeliveryOrder" class="box box-solid">
        @php 
            $data=['good_entry_id' => $good_entry->id]; 
        @endphp
            @include('detail_good_entries.create', $data)
        </div>
        @endif

        <div id="divDetailDeliveryOrder" class="box box-default">
            <div class="box-header">
                <h3 class="box-title">Detalle</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>         
            </div>
            <div class="box-body">
                <div class="table table-responsive">
                    <table class="table table-bordered table-striped table-hover" id="tbldetail_good_entry">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Cantidad</th>
                                <th>Material</th>
                                <th>Medida</th>
                                <th>Precio</th>
                                <th>Subtotal</th>
                                <th>{{ __('generic.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>@php $rows=0 @endphp
                        @foreach($detailGoodEntry as $key=>$item)
                            <tr class="detail_good_entry" data-id="{{ $item->id }}"> 
                                <td><a href="{!! route('detail_good_entry.show', [$item->id]) !!}">{{ $loop->iteration }}</a></td>
                                <td data-cell='DTA' data-format="0,0[.]00" >{{ $item->quantity }}</td>
                                <td>{{ $item->material }}</td>
                                <td>{{ $item->measure }}</td>
                                <td data-cell='DTB' data-format="0[.]00" >{{ $item->price }}</td>
                                <td data-cell="{{ 'DTC'.$loop->iteration}}" data-format="0.00" >{{ $item->subtotal }}</td>
                                <td>
                                    {!! Form::open(['route' => ['detail_good_entry.destroy', $item->id], 'method' => 'delete']) !!}
                                    <div class='btn-group'>
                                        <a href="{!! route('detail_good_entry.show', [$item->id]) !!}" 
                                        data-remote="false" data-toggle="modal" data-target="#detailDeliveryModal" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                        <a href="{!! route('detail_good_entry.edit', [$item->id]) !!}" class='btn btn-warning btn-xs DetailGoodEntry_edit'><i class="glyphicon glyphicon-edit"></i></a>
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs DetailDeliveryOrder_delete','data-id' => $item->id]) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </td>
                            </tr>@php $rows++ @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div> <!-- table-responsive !-->     
            </div> <!-- box-body !-->
            <div class="box-footer">
            </div>
        </div> <!-- box --!-->
</div> <!-- main wrapper !-->

    {!! Form::open(['url' => 'business_partners', 'class' => 'form-horizontal','id' => 'frmBusinessPartner','autocomplete' => 'off']) !!}
        @component('bootstrap::modal')
            @slot('id') customerModal @endslot
            @slot('title')
                <b>{{ __('generic.add_new') }} {{ __('business_partner.singular') }} </b>
            @endslot
            @slot('footer')
                {!! Form::button( __('business_partner.create'),['class' => 'btn btn-primary','id'=>'add_customer']) !!}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @endslot
        @endcomponent
    {!! Form::close() !!}

    {!! Form::open(['url' => 'business_partners', 'class' => 'form-horizontal','id' => 'formdetailDeliveryModal','autocomplete' => 'off']) !!}
        @component('bootstrap::modal')
            @slot('id') detailDeliveryModal @endslot
            @slot('title')
                <b>{{ __('good_entry.good_entry') }} </b>
            @endslot
            @slot('slot')
            <div class="content-fluid">
            </div>
            @endslot
            @slot('footer')
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @endslot
        @endcomponent
    {!! Form::close() !!}         

@endsection

@section('scripts')
<script src="{{ asset('/plugins/jquery-calx/numeral.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/jquery-calx/jquery-calx.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    numeral.language('gt', {
        delimiters: {
            thousands: ',',
            decimal: '.'
        },
        currency: {
            symbol: 'Q'
        }
    });

function initDropboxes () {
    //Initialize dynamic events

    $('#frmDetailGoodEntry').calx({
        readonly : true
    });

    $form = $('#formWrapper').calx({
        defaultFormat : '0[.]00',
        onBeforeCalculate : function(){
            cRows = $('#tbldetail_good_entry tbody tr').length;
            if (cRows>0){
                $('#formWrapper').calx('update');
                $('#formWrapper').calx('getCell', 'ST0').setFormula('SUM(DTC1:DTC'+cRows+')'); 
            }
          },        
        readonly : true
    });

    $('#material_id').select2({
    placeholder: "{{ __('form.type_selectdropdown') }}",
    allowClear:true,
    minimumInputLength: 2,
    theme: "bootstrap",
    templateResult: function(result) {
        var $result = $(
            '<div class="row">' +
                '<div class="col-md-2">' + result.code + '</div>' +
                '<div class="col-md-9">' + result.text + '</div>' +
            '</div>'
        );
        return $result;
    },
    //templateSelection: function(result) {},
    ajax: {
        url: "{{ URL::route('detail_good_entry.materials-autocomplete') }}",
        dataType: 'json',
        //delay: 250,
        processResults: function (data,page) {
            return {
            results:  $.map(data, function (item) {
                if  (item.name != '')
                   texto = item.name
                else
                   texto = item.description;

                    return {
                        id: item.id,
                        text: texto,
                        code: item.code,
                        name: item.name,
                        description: item.description
                    }
                })
            };
        },
        cache: true
        },
    });

}

$(document).ready(function() {
    initDropboxes();

    if  ($('#doc_date').val() == '' ) {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
        $('#doc_date').val(today);       
    }

    $('#business_partner_id').select2({
    placeholder: "{{ __('form.type_selectdropdown') }}",
    allowClear:true,
    minimumInputLength: 2,
    theme: "bootstrap",
    ajax: {
        url: "{{ URL::route('good_entry.providers-autocomplete') }}",
        dataType: 'json',
        //delay: 250,
        processResults: function (data) {
            return {
            results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                })
            };
        },
        cache: true
        }
    });

    $(document).on("click","#process", function(ev)  {
        ev.preventDefault();
        var datastring = $("#frmGoodEntry").serializeArray();

        $.ajax({
            type: 'GET',
            url: "{{ (isset($good_entry->id)) ? URL::route('good_entry.process', ['id'=>$good_entry->id]) : URL::route('good_entry.process') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(data) {
                if ((data.errors)){
                    var alertType = data.status_code;
                    var alertMessage = data.message;
                    var alerter = toastr[alertType];

                    if (alerter) {
                        $.each( data.errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });

                        alerter(alertMessage);
                    } else {
                        toastr.error("toastr alert-type " + alertType + " is unknown");
                    }

                }
                else {
                    toastr.success(" {{ __('delivery_order.success_processed') }} ");
                    window.location = data.url;
                }
            },

        });

    });

    $(document).on("click","#save_detailGoodEntry", function(e)  {
        e.preventDefault();

        var link = $(e.currentTarget);
        var action = $("#frmDetailGoodEntry").attr('action');
        var method = $("#frmDetailGoodEntry").attr('method');
        var datastring = $("#frmDetailGoodEntry").serialize();

        $.ajax({
        type: method,
        url: action,
        data: datastring,
        error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },        
        success: function(data){
            var deliveryOrder_id = '@if(isset($good_entry->id)){{ $good_entry->id }}@endif';
                        
            if ( link.data('id') ) {
                var cRow = link.data('id');
                var rowQuery = $('#tbldetail_good_entry tr[data-id="' + cRow + '"]');
                rowQuery.html("<td>" + (rowQuery.index()+1) + "</td>"+
                "<td data-cell='DTA"+cRow+"' >" + data.quantity + "</td>"+
                "<td>" + data.material + "</td>"+
                "<td>" + data.measure + "</td>"+
                "<td data-cell='DTB"+cRow+"' >" + data.price + "</td>"+
                "<td data-cell='DTC"+cRow+"' data-format='0.00' >" + data.subtotal + "</td>"+
                "<td><div class='btn-group'><a class='btn btn-info btn-xs' href='{{ URL::route('detail_good_entry.index')}}/"+data.id+"' data-id='" + data.id + "' data-remote='false' data-toggle='modal' data-target='#detailDeliveryModal' >"+
                "<span class='fa fa-eye'></span></a> <a href='{{ URL::route('detail_good_entry.index')}}/"+data.id+"/edit' class='btn btn-warning btn-xs DetailGoodEntry_edit' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-pencil'></span></a> <button class='btn btn-danger btn-xs DetailDeliveryOrder_delete' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-trash'></span></button></div></td>");

                var promise =  $.ajax({
                    url: "{{ URL::route('detail_good_entry.create') }}",
                    type: 'GET',
                    data: { good_entry_id: deliveryOrder_id } ,
                    contentType: 'application/json; charset=utf-8'
                })
                .done(function(data) {
                    $('#boxDetailDeliveryOrder').html(data);
                    toastr.success(" {{ __('generic.successfully_updated') }} ");
                });

                promise.then(function() {
                    initDropboxes();
                });

            } else {
                $itemlist = $('#tbldetail_good_entry');
                cRows = $itemlist.find('tbody tr').length;
                cRows++;
                $itemlist.append("<tr class='detail_good_entry' data-id='" + data.id + "'>"+
                "<td> <a href='{{ URL::route('detail_good_entry.index')}}/"+data.id+"' > " + cRows + "</a></td>"+
                "<td data-cell='DTA"+cRows+"' >" + data.quantity + "</td>"+
                "<td>" + data.material + "</td>"+
                "<td>" + data.measure + "</td>"+
                "<td data-cell='DTB"+cRows+"' >" + data.price + "</td>"+
                "<td data-cell='DTC"+cRows+"' data-format='0.00' >" + data.subtotal + "</td>"+
                "<td><div class='btn-group'><a class='btn btn-info btn-xs' href='{{ URL::route('detail_good_entry.index')}}/"+data.id+"' data-id='" + data.id + "' data-remote='false' data-toggle='modal' data-target='#detailDeliveryModal' >"+
                "<span class='fa fa-eye'></span></a> <a href='{{ URL::route('detail_good_entry.index')}}/"+data.id+"/edit' class='btn btn-warning btn-xs DetailGoodEntry_edit' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-pencil'></span></a> <button class='btn btn-danger btn-xs DetailDeliveryOrder_delete' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'>"+
                "<span class='glyphicon glyphicon-trash'></span></button></div></td>"+
                "</tr>");

                $form.calx('update');
                $form.calx('getCell', 'ST0').setFormula('SUM(DTC1:DTC'+cRows+')');
                $form.calx('getCell', 'ST0').calculate();

                var promise = $.ajax({
                    url: "{{ URL::route('detail_good_entry.create') }}",
                    type: 'GET',
                    data: { good_entry_id: deliveryOrder_id } ,
                    contentType: 'application/json; charset=utf-8'
                })
                .done(function(data) {
                    $('#boxDetailDeliveryOrder').html(data);
                    toastr.success(" {{ __('generic.successfully_added_new') }} ");
                });
                promise.then(function() {
                    initDropboxes();
                });

            }
        },
        });

    });

    $('#customerModal').on("show.bs.modal", function(e){
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $('#detailDeliveryModal').on("show.bs.modal", function(e){
        var link = $(e.relatedTarget);
        $(this).find(".modal-body .content-fluid").load(link.attr("href"));
    });

    $(document).on("click","#add_customer", function(ev)  {

        if(ev.target != this) return false;
        var datastring = $("#frmBusinessPartner").serialize();
        
        $.ajax({
            type: 'post',
            url: "{{ URL::route('business_partners.store') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(data) {
                if ((data.errors)){
                    var alertType = data.status_code;
                    var alertMessage = data.message;
                    var alerter = toastr[alertType];

                    if (alerter) {
                        $.each( data.errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });

                        alerter(alertMessage);
                    } else {
                        toastr.error("toastr alert-type " + alertType + " is unknown");
                    }

                }
                else {
                    toastr.success(" {{ __('good_entry.success_created') }} ");
                    $('#customerModal').modal('hide');
                }
            },

        });
        //$('#name').val('');
    });

    $(document).on("click",".DetailGoodEntry_edit", function(e)  {
        e.preventDefault();
        var link = $(this).attr('href')
        console.log(link);
        var datastring =""; 
        $.ajax({
            type: 'GET',
            url: link,
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                    console.log(xhr.responseText);
                }

            },                
            success: function(data){
                if (data) {
                cRows = $('#boxDetailDeliveryOrder').html(data);
                toastr.success(" {{ __('generic.all_done') }} ");
                initDropboxes();
                }

            },
            });            
    });

    $(document).on("click",".DetailDeliveryOrder_delete", function(e)  {
        e.preventDefault();
        var link = $(e.currentTarget);
        var row = $(this).parent().parent().parent();
        var r = confirm(" {{ __('generic.delete_question') }} ");
        
        if (r == true) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'DELETE',
                url: "{{ URL::route('detail_good_entry.index') }}/"+link.data('id'),
                data: { id: link.data('id') },
                error: function (xhr, status, errorThrown) {
                    var alertType = 'error';
                    var alertMessage = xhr.status + ' - ' + errorThrown;
                    var alerter = toastr[alertType];

                    if (xhr.status == '422') {
                        var $errors = xhr.responseJSON;
                        $.each( $errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });
                        alerter(alertMessage);
                    } else {
                        //Here the status code can be retrieved like;
                        alert(xhr.status + ' - ' + errorThrown );
                    }

                },
                success: function(data) {
                    if ((data.errors)){
                        var alertType = data.status_code;
                        var alertMessage = data.message;
                        var alerter = toastr[alertType];

                        if (alerter) {
                            $.each( data.errors, function( key, value ) {
                                alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                            });

                            alerter(alertMessage);
                        } else {
                            toastr.error("toastr alert-type " + alertType + " is unknown");
                        }

                    }
                    else {
                        $('tr*[data-id="'+ link.data('id') +'"]').remove();
                        $form.calx('update');
                        $form.calx('getCell', 'ST0').calculate();
                        toastr.success(" {{ __('generic.successfully_deleted') }} ");
                    }
                },

            });

        } else {
            toastr.warning(" {{ __('generic.error_deleting') }} ");
        }            

    });

});
</script>
@endsection