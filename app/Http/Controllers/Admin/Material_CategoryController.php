<?php

namespace App\Http\Controllers\Admin;

use App\Material_Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Voyager\VoyagerBaseController as BaseVoyagerController;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;

class Material_CategoryController extends BaseVoyagerController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $material_category = Material_Category::get()->toTree();
        return BaseVoyagerController::index($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return BaseVoyagerController::create($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->has('_validate')) {
            $inputs = $request->all();
            $node = new $dataType->model_name();
          //$node->parent_id = $inputs['parent_id'];

            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, $node);

            event(new BreadDataAdded($dataType, $data));

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
        }

        return BaseVoyagerController::store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material_Category  $material_Category
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        return BaseVoyagerController::show($request, $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material_Category  $material_Category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        return BaseVoyagerController::edit($request, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material_Category  $material_Category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

        return BaseVoyagerController::update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material_Category  $material_Category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        return BaseVoyagerController::destroy($request, $id);
    }
}
