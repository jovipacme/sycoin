<div class="table table-responsive">
    <table class="table table-bordered table-striped table-hover" id="tblgood_entry">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ __('good_entry.business_partner_id') }}</th>
                <th>{{ __('good_entry.doc_serie') }}</th>
                <th>{{ __('good_entry.doc_num') }}</th>
                <th>{{ __('good_entry.doc_date') }}</th>
                <th>{{ __('good_entry.actions') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($good_entry as $item)
            <tr>
                <td><a href="{{ url('good_entry', $item->id) }}">{{ $item->id }}</a></td>
                <td>
                    @if ($item->business_partner_id)
                        {{ $item->business_partner->name }}
                    @else
                        {{ $item->business_partner_id }}
                    @endif
                </td>
                <td>{{ $item->doc_serie }}</td>
                <td>{{ $item->doc_num }}</td>
                <td>{{ $item->doc_date->toFormattedDateString() }}</td>
                <td>
                    <a href="{{ url('good_entry/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{__('generic.update')}}</a> 
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['good_entry', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::submit( __('generic.delete') , ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>