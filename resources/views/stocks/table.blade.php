<div class="table table-responsive">
    <table class="table table-bordered table-striped table-hover" id="tblstocks">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ __('stocks.material_id') }}</th>
                <th>{{ __('stocks.mesure_id') }}</th>
                <th>Stock</th>
                <th>{{ __('stocks.storehouse') }}</th>
                <th>{{ __('stocks.locked') }}</th>
                <th>{{ __('stocks.actions') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($stocks as $item)
            <tr>
                <td><a href="{{ url('stocks', $item->id) }}">{{ $item->id }}</a></td>
                <td>
                    @if ($item->material_id)
                        {{ $item->material->name }}
                    @else
                        {{ $item->material_id }}
                    @endif                    
                </td>
                <td>
                    @if ($item->measure_id)
                        {{ $item->measure->name }}
                    @else
                        {{ $item->measure_id }}
                    @endif                    
                </td>
                <td>{{ $item->stock }}</td>
                <td>
                    @if ($item->storehouse_id)
                        {{ $item->storehouse->name }}
                    @else
                        {{ $item->storehouse_id }}
                    @endif
                </td>
                <td>{{ $item->locked }}</td>
                <td>
                    <a href="{{ url('stocks/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['stocks', $item->id],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>