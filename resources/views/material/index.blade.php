@extends('layouts.app')
@section('htmlheader_title')
{{ __('material.plural') }}
@stop

@section('main-content')

    <h1>{{ __('material.plural') }}<a href="{{ url('material/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} {{ __('material.singular') }}</a></h1>

    <div class="clearfix"></div>
    <div class="box box-default">
        <div class="box-body">
                @include('material.table')
        </div>
    </div>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        $('#tbladmin_material').DataTable({
            order: [],
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}
        });

    });
</script>
@endsection