<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Material_Provider;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class Material_ProvidersController extends Controller
{
    private $business_partner;

    public function __construct()
    {   //Assign constant for filter only Providers
        $this->business_partner = config('constants.BusinessPartner_Category.provider');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $material_providers = Material_Provider::all();

        return view('material_providers.index', compact('material_providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $materialList = \App\Material::pluck('name', 'id');
        $providerList = \App\Business_Partner::where('category_id','=', $this->business_partner)->pluck('name', 'id');
        $measureList = \App\Measure::pluck('name', 'id');
        return view('material_providers.create', compact('materialList','providerList','measureList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['material_id' => 'required', 'business_partner_id' => 'required', ]);

        Material_Provider::create($request->all());

        toast()->success( __('material_providers.error_created'), __('material.message'));
       

        return redirect('material_providers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $material_provider = Material_Provider::findOrFail($id);

        return view('material_providers.show', compact('material_provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $material_provider = Material_Provider::findOrFail($id);

        $materialList = \App\Material::pluck('name', 'id');
        $providerList = \App\Business_Partner::where('category_id','=', $this->business_partner)->pluck('name', 'id');
        $measureList = \App\Measure::pluck('name', 'id');

        return view('material_providers.edit', compact('material_provider','materialList','providerList','measureList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['material_id' => 'required', 'business_partner_id' => 'required', ]);

        $material_provider = Material_Provider::findOrFail($id);
        $material_provider->update($request->all());

        toast()->success( __('material_providers.error_updated'), __('material.message'));

        return redirect('material_providers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material_provider = Material_Provider::findOrFail($id);

        $material_provider->delete();

        toast()->success( __('material_providers.error_deleted'), __('material.message'));

        return redirect('material_providers');
    }

}
