<?php

return [
	'BusinessPartner_Category' => array(
		'provider'   => 1,
		'customer' => 2,
		'member' => 3,
	),

	'DocumentStatus' => array(
		'open'   => 1,
		'canceled' => 2,
		'pending' => 3,
		'closed' => 4,
	),	
];