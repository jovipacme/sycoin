<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('materials', function(Blueprint $table) {
                $table->increments('id');
                $table->string('code',50)->nullable();
                $table->string('name');
                $table->text('description')->nullable();
                $table->integer('material_category_id')->unsigned()->nullable();

                $table->timestamps();
                $table->softDeletes();

                
            });
            
            Schema::table('materials', function ($table) {
                $table->foreign('material_category_id')->references('id')->on('material_categories');
            });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }

}
