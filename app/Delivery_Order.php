<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;
use App\Traits\DatesTranslator;
class Delivery_Order extends BaseModel
{
    use SoftDeletes, SoftCascadeTrait, DatesTranslator;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'delivery_orders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['business_partner_id', 'user_id', 'partner_document_id', 'doc_serie', 'doc_num', 'doc_date', 'total', 'status_id'];

    protected $dates = ['doc_date','deleted_at'];

    protected $casts = [
        'total' => 'float(10,2)',
    ];

    public function business_partner() {
        return $this->belongsTo('App\Business_Partner','business_partner_id');
    } 

    public function details_delivery_order() {
        return $this->hasMany('App\Detail_Delivery_Order');
    }

    public function status_description($status_id) {
        return \App\Document_Status::label($status_id);
    }

    public function getDocDateAttribute($value){
        return new \Date($value);
    }

}
