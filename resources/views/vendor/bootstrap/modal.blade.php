<div
    class="modal {{ $animation or 'fade' }} {{ $class or '' }}"
    id="{{ $id or 'modal' }}"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        @isset($head_form)
            {{ $head_form }}
        @endisset
            @isset($title)
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title">{{ $title }}</h5>
                </div>
            @endisset

            <div class="modal-body">
                {{ $slot }}
            </div>

            @isset($footer)
                <div class="modal-footer">
                    {{ $footer }}
                </div>
            @endisset
        @isset($footer_form)
            {{ $footer_form }}
        @endisset
        </div>
    </div>
</div>