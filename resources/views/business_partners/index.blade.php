@extends('layouts.app')
@section('htmlheader_title')
{{ __('business_partner.plural') }}
@stop

@section('main-content')

    <h1>{{ __('business_partner.plural') }} <a href="{{ url('business_partners/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} {{ __('business_partner.singular') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblbusiness_partners">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ __('business_partner.name') }}</th>
                    <th>{{ __('business_partner.telephone') }}</th>
                    <th>{{ __('business_partner.email') }}</th>
                    <th>{{ __('business_partner.tags') }}</th>
                    <th>{{ __('business_partner.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($business_partners as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('business_partners', $item->id) }}">{{ $item->name }}</a></td>
                    <td>{{ $item->telephone }}</td>
                    <td>{{ $item->email }}</td>
                    <td>
                        @forelse($item->tags as $tag)
                        <span class="label label-info"> {{ $tag->name }} </span> &nbsp;
                        @empty
                            &nbsp;
                        @endforelse
                    </td>
                    <td>
                        <a href="{{ url('business_partners/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">{{ __('generic.update') }}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['business_partners', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblbusiness_partners').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection