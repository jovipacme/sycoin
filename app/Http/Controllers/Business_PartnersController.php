<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Business_Partner;
use App\Partner_Category;
use App\Identity_Document;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class Business_PartnersController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'name' => 'required',
        'telephone' => "nullable|regex:/^([\d-\s,]*)+$/",
        'email' => 'nullable|email',
        'category_id' => 'required',
        'identity_document'=>'nullable|alpha_dash'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $business_partners = Business_Partner::all();

        return view('business_partners.index', compact('business_partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categoriesList = Partner_Category::pluck('name', 'id');
        
        $identityDocuments = Identity_Document::select("identity_documents.id",
        "identity_documents.name AS identity_document","partner_categories.name AS partner_category")
        ->join('partner_categories', 'partner_categories.id', '=', 'identity_documents.partner_category_id')
        ->get()->map(function($items){
            $data['id'] = $items->id;
            $data['full_description'] = trim(sprintf('%s %s', $items->partner_category, $items->identity_document));
            return $data;
         });
         $identityDocumentsList = $identityDocuments->pluck('full_description', 'id');

        return view('business_partners.create', compact('categoriesList','identityDocumentsList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            toast()->clear();
            $validation->validate();

        }
            $tags = array_filter(explode(',', $request->tags), 'strlen');

            $data = Business_Partner::create($request->all());
            if (empty($tags)==false ) {
                $data->tag($tags);
            }

            //Si es una peticion ajax
            if( $request->ajax() ){
                return response()->json($data);
            } else {

                toast()->success( __('business_partner.error_created'), __('material.message'));
        
                return redirect('business_partners');
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $business_partner = Business_Partner::findOrFail($id);

        $tagged = $business_partner->tagged->pluck('tag_name')->all();
        $tags = implode(',', $tagged);
        $business_partner->tagsList = $tags;

        return view('business_partners.show', compact('business_partner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $business_partner = Business_Partner::findOrFail($id);
        $tagged = $business_partner->tagged->pluck('tag_name')->all();
        $tags = implode(',', $tagged);
        $business_partner->tagsList = $tags;
        $categoriesList = Partner_Category::pluck('name', 'id');
        
        $identityDocuments = Identity_Document::select("identity_documents.id",
        "identity_documents.name AS identity_document","partner_categories.name AS partner_category")
        ->join('partner_categories', 'partner_categories.id', '=', 'identity_documents.partner_category_id')
        ->get()->map(function($items){
            $data['id'] = $items->id;
            $data['full_description'] = trim(sprintf('%s %s', $items->partner_category, $items->identity_document));
            return $data;
         });
         $identityDocumentsList = $identityDocuments->pluck('full_description', 'id');

        return view('business_partners.edit', compact('business_partner','categoriesList','identityDocumentsList','tagged'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }

        $tags = array_filter(explode(',', $request->tagsList), 'strlen');

        $business_partner = Business_Partner::findOrFail($id);
        $business_partner->update($request->all());

        if (empty($tags)==false ) {
            $business_partner->retag($tags);
        }        

        toast()->success( __('business_partner.error_updated'), __('material.message'));

        return redirect('business_partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $business_partner = Business_Partner::findOrFail($id);

        $business_partner->delete();

        toast()->success( __('business_partner.error_deleted'), __('material.message'));

        return redirect('business_partners');
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataIdentityDocument(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = Identity_Document::
                    select("identity_documents.id","identity_documents.name AS identity_document","partner_categories.name AS partner_category")
                    ->join('partner_categories', 'partner_categories.id', '=', 'identity_documents.partner_category_id')
                    ->where('identity_documents.name','LIKE',"%$search%")
                    //->orWhere('identity_documents.description','LIKE',"%$search%")
                    ->get();
        }
        if($request->has('material_id')){
            $data = Identity_Document::
                    select("id","name","description")
                    ->where('partner_category_id','=',$request->material_id)
                    ->get();       
        }
        return response()->json($data);
    } 
}
