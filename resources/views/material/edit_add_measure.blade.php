<div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
        <div class="col-sm-6">
        {!! Form::hidden('material_id', $material_id, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
    {!! Form::label('measure_id', __('material_measures.measure_id'), ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('measure_id',$measuresList, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>