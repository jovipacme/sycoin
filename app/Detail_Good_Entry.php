<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detail_Good_Entry extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detail_good_entries';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['good_entry_id', 'material_id','measure_id', 'quantity', 'price', 'subtotal'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function good_entry()
    {
        return $this->belongsTo('App\Good_Entry','good_entry_id');
    }

    public function material() {
        return $this->belongsTo('App\Material','material_id');
    }

    public function measure() {
        return $this->belongsTo('App\Measure','measure_id');
    }    

}
