<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIdentityDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('identity_documents', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name',50);
                $table->string('description');
                $table->integer('partner_category_id')->unsigned();
                $table->unique(['name', 'partner_category_id']);
                $table->timestamps();
                $table->softDeletes();
            });

            Schema::table('identity_documents', function(Blueprint $table) 
            {
                $table->foreign('partner_category_id')->references('id')->on('partner_categories');
            });               
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identity_documents');
    }

}
