@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_measures.singular') }}
@stop

@section('main-content')

    <h1>{{ __('material_measures.singular') }} 
        {!! Form::open([
                'id'=>'measureAdd',
                'method'=>'GET',
                'url' => route('material_measures.create'),
                'style' => 'display:inline'
            ]) !!}
            {!! Form::hidden('material_id', null, ['id' => 'material_id','class' => 'form-control']) !!}
            {!! Form::submit( __('generic.add_new'), ['class' => 'btn btn-primary pull-right btn-sm','data-remote'=>'false',
                'data-toggle'=>'modal','data-target'=>'#measureModal','data-mode'=>'create']) !!}
        {!! Form::close() !!}        
    </h1>
    <div class="clearfix"></div>
    <div class="box box-default">
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="tblmaterial_measures">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ __('material_measures.material') }}</th>
                        <th>{{ __('material_measures.measure') }}</th>
                        <th>{{ __('material_measures.unit') }}</th>
                        <th>{{ __('material_measures.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="measureModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div>   

@endsection

@section('scripts')

<script type="text/javascript">
    var tblmaterial_measures = null;
    $(document).ready(function(){

        $('#measureAdd').on('submit', function(e){
            e.preventDefault();
        });

        $('#measureModal').on("show.bs.modal", function(e){
            var mode = $(e.relatedTarget).data('mode');

            switch(mode) {
            case 'create':
                ajaxLoad( $(e.relatedTarget).closest("form").attr('action'), $('#measureAdd').serializeArray(), $(this).find(".modal-content"), mode );                
                break;
            case 'edit':
                ajaxLoad( $(e.relatedTarget).attr('href'), $('#measureAdd').serializeArray(), $("#measureModal .modal-content"), mode );
                break;
            };

        });

        tblmaterial_measures = $('#tblmaterial_measures').dataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:  '{{ route('material_measures.list') }}',
                data: {
                    "material_id": $('#material_id').val()
                }
            },
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                        { data: 'material', name: 'material' },
                        { data: 'measure', name: 'measure' },
                        { data: 'unit', name: 'unit' },                        
                        { data: 'action', name: 'action' }
            ],            
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[1, "asc"]],
            language: {!! json_encode(__('datatable')) !!}            
        });

        function ajaxLoad(filename, parameters, container, mode) {
            container = typeof container !== 'undefined' ? container : '#container';
            $('.loading').show();
            $.ajax({
                type: "GET",
                url: filename,
                data: parameters,
                contentType: false,
                success: function (data) {
                    $(container).html(data);
                    $('.loading').hide();
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }

        $(document).on("click","#add_measure", function(ev)  {

            if(ev.target != this) return false;
            var datastring = $("#frmMaterialMeasure").serializeArray();

            $.ajax({
                type: 'post',
                url: "{{ URL::route('material_measures.store') }}",
                data: datastring,
                error: function (xhr, status, errorThrown) {
                    var alertType = 'error';
                    var alertMessage = xhr.status + ' - ' + errorThrown;
                    var alerter = toastr[alertType];

                    if (xhr.status == '422') {
                        var $errors = xhr.responseJSON;
                        $.each( $errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });
                        alerter(alertMessage);
                    } else {
                        //Here the status code can be retrieved like;
                        alert(xhr.status + ' - ' + errorThrown );
                    }

                },
                success: function(data) {
                    if ((data.errors)){
                        var alertType = data.status_code;
                        var alertMessage = data.message;
                        var alerter = toastr[alertType];

                        if (alerter) {
                            $.each( data.errors, function( key, value ) {
                                alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                            });

                            alerter(alertMessage);
                        } else {
                            toastr.error("toastr alert-type " + alertType + " is unknown");
                        }

                    }
                    else {
                        tblmaterial_measures.DataTable().api().ajax.reload();
                        toastr.success(" {{ __('material_measures.success_created') }} ");
                        $('#measureModal').modal('hide');
                    }
                },

            });

        });

        $(document).on("click","#edit_measure", function(ev)  {

            if(ev.target != this) return false;
            var cRows = $(ev.currentTarget).data('id');
            
            $.ajax({
                type: 'post',
                url: $("#frmMaterialMeasure").prop('action') ,
                data: $("#frmMaterialMeasure").serializeArray(),
                error: function (xhr, status, errorThrown) {
                    var alertType = 'error';
                    var alertMessage = xhr.status + ' - ' + errorThrown;
                    var alerter = toastr[alertType];

                    if (xhr.status == '422') {
                        var $errors = xhr.responseJSON;
                        $.each( $errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });
                        alerter(alertMessage);
                    } else {
                        //Here the status code can be retrieved like;
                        alert(xhr.status + ' - ' + errorThrown );
                    }

                },
                success: function(data) {
                    if ((data.errors)){
                        var alertType = data.status_code;
                        var alertMessage = data.message;
                        var alerter = toastr[alertType];

                        if (alerter) {
                            $.each( data.errors, function( key, value ) {
                                alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                            });

                            alerter(alertMessage);
                        } else {
                            toastr.error("toastr alert-type " + alertType + " is unknown");
                        }

                    }
                    else {
                        tblmaterial_measures.DataTable().api().ajax.reload();             
                        toastr.success(" {{ __('material_measures.success_created') }} ");
                        $('#measureModal').modal('hide');
                    }
                },

            });

        });

    });
</script>
@endsection