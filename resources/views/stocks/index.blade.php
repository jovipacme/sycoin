@extends('layouts.app')
@section('htmlheader_title')
Stock
@stop

@section('main-content')

    <h1>Stocks <a href="{{ url('stocks/create') }}" class="btn btn-primary pull-right btn-sm">Add New Stock</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblstocks">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{ __('stocks.material_id') }}</th>
                    <th>{{ __('stocks.mesure_id') }}</th>
                    <th>{{ __('stocks.stock') }}</th>
                    <th>{{ __('stocks.storehouse') }}</th>
                    <th>{{ __('stocks.locked') }}</th>
                    <th>{{ __('stocks.actions') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        tblstocks = $('#tblstocks').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:  '{{ route('stocks.list') }}',
                data: {
                    "material_id": $('#material_id').val()
                }
            },
            columns: [
                        { data: 'DT_RowIndex', name: 'id' },
                        { data: 'material', name: 'material' },
                        { data: 'measure', name: 'measure' },
                        { data: 'stock', name: 'stock' },
                        { data: 'storehouse', name: 'storehouse' },
                        { data: 'locked', name: 'locked' },                                                 
                        { data: 'action', name: 'action' }
            ], 
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection