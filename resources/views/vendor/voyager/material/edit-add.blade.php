@extends('voyager::master')

@section('page_title', __('generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ (isset($dataTypeContent->id)) ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                    {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                                {!! Form::label('code', __('generic.code'), ['class' => 'col-sm-3 control-label']) !!}
                                <div class="">
                                    @php $code = (isset($dataTypeContent->code)) ? $dataTypeContent->code : ''  @endphp
                                    {!! Form::text('code', $code, ['class' => 'form-control']) !!}
                                    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                {!! Form::label('description', __('generic.description') , ['class' => 'control-label']) !!}
                                <div class="">
                                    @php $description = (isset($dataTypeContent->description)) ? $dataTypeContent->description : ''  @endphp
                                    {!! Form::textarea('description', $description, ['class' => 'form-control', 'required' => 'required', 'rows' => '2']) !!}
                                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('material_category_id') ? 'has-error' : ''}}">
                                {!! Form::label('material_category_id', 'Material Category Id: ', ['class' => 'control-label']) !!}
                                <div class="">
                                    @php $material_category_id = (isset($dataTypeContent->material_category_id)) ? $dataTypeContent->material_category_id : ''  @endphp
                                    {!! Form::select('material_category_id', $categoriesList, $material_category_id, ['class' => 'form-control']) !!}
                                    {!! $errors->first('material_category_id', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary pull-right save">
                        {{ __('generic.save') }}
                    </button>
                </div>            
            </div>
        </form>
        
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
