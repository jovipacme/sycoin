<a href="{{ route('good_entry.edit', ['id'=> $item->id] ) }}" class="btn btn-primary btn-xs edit_good_entry" >
    {{ __('generic.update') }}
</a> 
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['good_entry', $item->id ],
    'style' => 'display:inline'
]) !!}
    {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
{!! Form::close() !!}