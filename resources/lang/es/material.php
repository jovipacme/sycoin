<?php

return [
    'plural'                => 'Materiales',
    'singular'              => 'Material',
    'create'                => 'Crear',
    'actions'               => 'Accion',
    'update'                => 'Actualizar',
    'code'                  => 'Codigo',
    'description'           => 'Descripcion',
    'name'                  => 'Nombre material',
    'prices'                => 'Precios',
    'measures'              => 'Medidas',
    'providers'             => 'Proveedores',        
    'category_material_id'  => 'ID Categoria material',
    'category_material'     => 'Categoria material',
    'message'               => 'Mensaje',
    'error_creating'        => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'        => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'        => 'Lo siento, parece que hubo un problema al actualizar',
    'error_created'         => 'Material creado exitosamente',
    'error_deleted'         => 'Material borrado exitosamente',
    'error_updated'         => 'Material se actualizo correctamente',
   ];
