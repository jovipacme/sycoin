<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNestedToMaterialCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('material_categories')) {
            Schema::table('material_categories', function (Blueprint $table) {
                $table->integer('parent_id')->unsigned()->nullable()->after('name');
                $table->integer('_lft')->unsigned();
                $table->integer('_rgt')->unsigned();

                $table->foreign('parent_id')->references('id')->on('material_categories');
            });
        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('material_categories')) {
            Schema::table('material_categories', function (Blueprint $table) {
                $table->dropForeign('material_categories_parent_id_foreign');
                $table->dropColumn(['parent_id', '_lft', '_rgt']);
            });
        };
    }
}
