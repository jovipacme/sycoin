<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterIdForBusinessPartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('nit', 'business_partners') == false) {
            Schema::table('business_partners', function (Blueprint $table) {
                $table->string('nit',191)->change();
                $table->renameColumn('nit', 'identity_document');
                $table->integer('identity_document_id')->unsigned()->nullable()->after('category_id');
            });

            Schema::table('business_partners', function(Blueprint $table) 
            {
                $table->foreign('identity_document_id')->references('id')->on('identity_documents');
            });             
        }
        if (Schema::hasColumn('email', 'business_partners') == false) {
            Schema::table('business_partners', function (Blueprint $table) {
                $table->string('email',80)->change();
                $table->string('contact')->nullable()->after('email');
            });            
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('nit', 'business_partners')) {
            Schema::table('material_providers', function (Blueprint $table) {
                $table->text('nit')->change();
                $table->renameColumn('identity_document', 'nit');
                $table->dropColumn('identity_document_id');
            });
        }
        if (Schema::hasColumn('contact', 'business_partners')) {
            Schema::table('material_providers', function (Blueprint $table) {
                $table->dropColumn('contact');
            });
        } 
    }
}
