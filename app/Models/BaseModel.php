<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;
use Fico7489\Laravel\RevisionableUpgrade\Traits\RevisionableUpgradeTrait;

abstract class BaseModel extends Model
{
    use RevisionableTrait;
    use RevisionableUpgradeTrait;

    protected $revisionCreationsEnabled = true; //enable this if you want use methods that gets information about creating
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 100; //Maintain a maximum of 100 changes at any point of time, while cleaning up old revisions.    

   /**
     * Converts the created_at to Local-format
     *
     * @return string
     */
    public function getCreatedAtLocaleAttribute()
    {
        return $this->created_at->isoFormat('L LTS');
    }

    /**
     * Converts the updated_at to Local-format
     *
     * @return string
     */
    public function getUpdatedAtLocaleAttribute()
    {
        return $this->updated_at->isoFormat('L LTS');
    }

    /**
     * Converts the deleted_at to Local-format
     *
     * @return string
     */
    public function getDeletedAtLocaleAttribute()
    {
        return $this->deleted_at->isoFormat('L LTS');
    }      
}
