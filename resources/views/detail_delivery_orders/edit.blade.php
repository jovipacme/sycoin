<div class="box-body">
    <div class="col-lg-12">
        {!! Form::model($detailDeliveryOrder, ['route' => ['detail_delivery_order.update', $detailDeliveryOrder->id], 'method' => 'patch',
            'class' => 'form-horizontal','id'=>'frmDetailDeliveryOrder','autocomplete' => 'off']) !!}
        <div class="row">   
            @include('detail_delivery_orders.fields')
        </div>
        {!! Form::close() !!}
    </div> <!-- col-12 !-->
</div> <!-- box body !-->