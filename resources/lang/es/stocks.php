<?php

return [
    'material_id'            => 'ID Material',
    'mesure_id'              => 'ID Medida',
    'stock'                  => 'Existencia',    
    'storehouse'             => 'Almacen',
    'locked'                 => 'Bloqueado',
    'actions'                => 'Accion'
];
