<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;
use App\Traits\DatesTranslator;
class Good_Entry extends BaseModel
{
    use SoftDeletes, SoftCascadeTrait, DatesTranslator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'good_entries';

    /**
     *  Attributes that can search.
     *
     * @var string
     */

    protected $searchableColumns = ['name'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['business_partner_id', 'user_id', 'partner_document_id', 'doc_serie', 'doc_num', 'doc_date', 'total', 'status_id'];

    protected $dates = ['doc_date','deleted_at'];

    protected $casts = [
        'total' => 'float(10,2)',
    ];

    public function business_partner() {
        return $this->belongsTo('App\Business_Partner','business_partner_id');
    } 

    public function details_good_entry() {
        return $this->hasMany('App\Detail_Good_Entry');
    }
    
    public function status_description($status_id) {
        return \App\Document_Status::label($status_id);
    }
    
    public function getDocDateLocaleAttribute() {
        return $this->doc_date->isoFormat('L');
    }    
}
