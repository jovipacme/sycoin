<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\PreviousRoute;
use App\Detail_Delivery_Order;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class Detail_Delivery_OrderController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'delivery_order_id' => 'required',
        'material_id' => 'required',
        'measure_id' => 'required',
        'quantity' => 'numeric',
        'price' => 'numeric',
        'subtotal'=>'numeric'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $detailDeliveryOrders = Detail_Delivery_Order::all();

        PreviousRoute::setNamedRoute('session.index.back');
        return view('detail_delivery_orders.index', compact('detailDeliveryOrders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $materialList = array();
        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        return view('detail_delivery_orders.create',compact('materialList','measureList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        } else{
            toast()->clear();
        }

        $data = Detail_Delivery_Order::create($request->all());

        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Detail_Delivery_Order::select("detail_delivery_orders.id","detail_delivery_orders.delivery_order_id",
            "materials.name AS material","measures.name AS measure","detail_delivery_orders.quantity","detail_delivery_orders.price",
            "detail_delivery_orders.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_delivery_orders.material_id')
            ->join('measures', 'measures.id', '=', 'detail_delivery_orders.measure_id')
            ->where('detail_delivery_orders.id','=',$data->id)
            ->first();

            return response()->json($detail);
        } else {
            toast()->success('message', 'Detail_Delivery_Order added!');
    
            return redirect('detail_delivery_order');
        }        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        $detailDeliveryOrder = Detail_Delivery_Order::findOrFail($id);

        if( $request->ajax() ){
            //return response()->json($detailDeliveryOrder);
            return view('detail_delivery_orders.show_fields', compact('detailDeliveryOrder'));
        } else {
            return view('detail_delivery_orders.show', compact('detailDeliveryOrder'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        if (empty($id)==false) {
            
            $detailDeliveryOrder = Detail_Delivery_Order::select("detail_delivery_orders.id","detail_delivery_orders.delivery_order_id",
            "detail_delivery_orders.material_id","materials.name AS material","measures.name AS measure","detail_delivery_orders.quantity","detail_delivery_orders.price",
            "detail_delivery_orders.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_delivery_orders.material_id')
            ->join('measures', 'measures.id', '=', 'detail_delivery_orders.measure_id')
            ->where('detail_delivery_orders.id','=',$id)->firstOrFail();

            $materialList = \App\Material::select("id","code","name","description")
                ->where('id','=',$detailDeliveryOrder['material_id'])
                ->pluck('name', 'id');

            $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        } else {
            $detailDeliveryOrder = Detail_Delivery_Order::findOrFail($id);
            $materialList = array();
            $measureList = array();
        }
        

        return view('detail_delivery_orders.edit', compact('detailDeliveryOrder','materialList','measureList'));

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id,Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        } else{
            toast()->clear();
        }

        $detailDeliveryOrders = Detail_Delivery_Order::findOrFail($id);
        $detailDeliveryOrders->update($request->all());

        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Detail_Delivery_Order::select("detail_delivery_orders.id","detail_delivery_orders.delivery_order_id",
            "materials.name AS material","measures.name AS measure","detail_delivery_orders.quantity","detail_delivery_orders.price",
            "detail_delivery_orders.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_delivery_orders.material_id')
            ->join('measures', 'measures.id', '=', 'detail_delivery_orders.measure_id')
            ->where('detail_delivery_orders.id','=',$id)
            ->first();

            toast()->success('message', 'Detail_Delivery_Order updated!');
            return response()->json($detail);
        } else {
            
            return redirect('detail_delivery_order');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy(Request $request,$id)
    {
        $detailDeliveryOrders = Detail_Delivery_Order::findOrFail($id);

        $detailDeliveryOrders->delete();

        //Si es una peticion ajax
        if( $request->ajax() ){
            return response()->json($detailDeliveryOrders);
        } else {
        Session::flash('message', 'Detail_Delivery_Order deleted!');
        Session::flash('status', 'success');
        }
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataMaterials(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
        /*
            $data = DB::table("materials")
                    ->select("id","code","name","description")
                    ->where('description','LIKE',"%$search%")
                    ->where('deleted_at','=',Null)
                    ->get();
        */
            $data = \App\Material::search($search)->get();

        }
        return response()->json($data);
    } 

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataMeasures(Request $request)
    {
        $data = [];

        if($request->has('material_id')){
            $search = $request->material_id;
            $data = DB::table("material_measures")
                    ->select("measures.id","measures.name","measures.unit")
                    ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
                    ->where('material_measures.material_id','=',$request->material_id)
                    ->where('material_measures.deleted_at','=',Null)                    
                    ->get();
        }
        return response()->json($data);
    } 

}
