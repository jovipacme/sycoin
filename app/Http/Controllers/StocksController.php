<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stock;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Session;
use DB;

class StocksController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $stocks = Stock::all();

        return view('stocks.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $materialsList = \App\Material::select("id","name","description")
            ->where('deleted_at','=',NULL)
            ->pluck('name', 'id');

        $materialMeasuresList = [];

        $storehousesList = \App\Storehouse::select('id','name','description')
            ->where('deleted_at','=',NULL)
            ->where('locked','=',false)
            ->pluck('name', 'id');

        return view('stocks.edit-add',compact('materialsList','materialMeasuresList','storehousesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['material_id' => 'required','measure_id' => 'required', 'stock' => 'required', ]);

        Stock::create($request->all());

        Session::flash('message', 'Stock added!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);

        return view('stocks.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stock = Stock::findOrFail($id);

        $materialsList = \App\Material::select("id","name","description")
            ->where('deleted_at','=',NULL)
            ->pluck('name', 'id');
        
        $materialMeasuresList = \App\Material_Measure::select( 'material_measures.material_id','materials.description as material',
            'material_measures.measure_id','measures.name as medida','material_measures.id',
            DB::raw("CONCAT(materials.name,' - ',measures.name) AS full_description") )
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')    
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
            ->where('materials.id','=', $stock->material_id)
            ->where('material_measures.deleted_at','=', NULL)
            ->orderBy('material_measures.material_id', 'asc')
            ->orderBy('material_measures.measure_id', 'asc')
            ->pluck('full_description','material_measures.measure_id');

        $storehousesList = \App\Storehouse::select('id','name','description')
            ->where('deleted_at','=',NULL)
            ->where('locked','=',false)
            ->pluck('name', 'id');

        return view('stocks.edit-add', compact('stock','materialsList','materialMeasuresList','storehousesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['material_id' => 'required','measure_id' => 'required', 'stock' => 'required', ]);

        $stock = Stock::findOrFail($id);
        $stock->update($request->all());

        Session::flash('message', 'Stock updated!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);

        $stock->delete();

        Session::flash('message', 'Stock deleted!');
        Session::flash('status', 'success');

        return redirect('stocks');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function list(Request $request)
    {   
        if ($request->exists('material_id')==true && !empty($request->material_id)) {
            $stocks = Stock::select("stocks.id","materials.name AS material","measures.name AS measure","stocks.stock","storehouses.name AS storehouse","stocks.locked")
            ->join('materials', 'materials.id', '=', 'stocks.material_id')
            ->join('measures', 'measures.id', '=', 'stocks.measure_id')
            ->leftJoin('storehouses', 'storehouses.id', '=', 'stocks.storehouse_id')
            ->where('material_id',$request->material_id)->get();
        } else {
            $stocks = Stock::select("stocks.id","materials.name AS material","measures.name AS measure","stocks.stock","storehouses.name AS storehouse","stocks.locked")
            ->join('materials', 'materials.id', '=', 'stocks.material_id')
            ->join('measures', 'measures.id', '=', 'stocks.measure_id')
            ->leftJoin('storehouses', 'storehouses.id', '=', 'stocks.storehouse_id')
            ->get();
        }
        if ($request->has('showDeleted') && $request->get('showDeleted') == 1) {
            $stocks = $stocks->withTrashed();
        }

        return Datatables::of($stocks)
            ->setRowClass(function ($item) {
                return "stocks" . ($item->trashed() ? ' danger' : '');
            })           
            ->setRowData([
                'data-id' => function($item) {
                    return $item->id;
                }
            ])
            ->editColumn('locked', function($item) {
                $value = ($item->locked==1)? __('generic.yes'):__('generic.no');
                return $value;
            })            
            ->addColumn('action', function($item) {
                return view('stocks.list_actions', compact('item'))->render();
            })
            ->addIndexColumn()
            ->make(true);
            
    }    

}
