<?php

return [
    'documents_plural'                      => 'Documentos para socios de negocio',
    'document_singular'                     => 'documento para socio de negocio',
    'document'                              => 'documento',
    'name'                                  => 'Nombre',
    'category_business_partner_id'          => 'ID Categoria socio de negocio',
    'actions'                               => 'Accion',
    'create'                                => 'Crear',
    'business_partner_id'                   => 'ID socio de negocio',
    'error_creating'                        => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'                        => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'                        => 'Lo siento, parece que hubo un problemaa al actualizar',
    'error_created'                         => 'Socio de negocio creado exitosamente',
    'error_deleted'                         => 'Socio de negocio eliminado exitosamente',
    'error_updated'                         => 'Socio de negocio se actualizo correctamente',
   ];
