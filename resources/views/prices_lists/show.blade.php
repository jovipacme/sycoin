@extends('layouts.app')
@section('htmlheader_title')
Pricelist
@stop

@section('main-content')

    <h1>Pricelist</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>User Id</th><th>Percent Profit</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $pricelist->id }}</td> <td> {{ $pricelist->name }} </td><td> {{ $pricelist->user_id }} </td><td> {{ $pricelist->percent_profit }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection