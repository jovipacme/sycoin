@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('material.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('material.singular') }}</h1>
    <hr/>

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="{{ Route::is('material.edit') ? 'active' : '' }}"><a href="#tab_1" data-toggle="tab">{{ __('material.singular') }}</a></li>
        <li><a href="#tab_2" data-toggle="tab" data-url="{{ route('material_measures.create') }}">{{ __('material_measures.plural') }}</a></li>
        <li><a href="#tab_3" data-toggle="tab">{{ __('stocks.plural') }}</a></li>
        <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane {{ Route::is('material.edit') ? 'active' : '' }}" id="tab_1">

            {!! Form::model($material, [
                'id'=>'frmMeasure',
                'method' => 'PATCH',
                'data-id' => $material->id,
                'url' => ['material', $material->id],
                'class' => 'form-horizontal'
            ]) !!}

                    <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
                        {!! Form::label('code', __('material.code'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('code', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                        {!! Form::label('name', __('material.name'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                        {!! Form::label('description', __('material.description'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '2']) !!}
                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('material_category_id') ? 'has-error' : ''}}">
                        {!! Form::label('material_category_id', __('material.category_material_id'), ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            {!! Form::select('material_category_id', $categoriesList, null, ['class' => 'form-control']) !!}
                            {!! $errors->first('material_category_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    {!! Form::submit( __('material.update'), ['class' => 'btn btn-primary form-control']) !!}
                </div>
                <div class="col-sm-3">
                    {{ link_to(PreviousRoute::getNamedRoute('session.index.back'), __('generic.cancel'), ['class' => 'btn btn-primary form-control']) }}
                </div>        
            </div>
            {!! Form::close() !!}

        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            <a href="{{ route('material.addMeasure',['material_id'=>$material->id]) }}" data-remote="false" data-toggle="modal" data-target="#measureModal" 
                class="btn btn-primary pull-right btn-sm"><i class="glyphicon glyphicon-plus"></i> {{__('generic.add')}}
            </a>
            <div class="table table-responsive">
                <table class="table table-bordered table-striped table-hover" id="tblmaterial_measures">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{ __('material_measures.material') }}</th>
                            <th>{{ __('material_measures.measure') }}</th>
                            <th>{{ __('material_measures.unit') }}</th>
                            <th>{{ __('material_measures.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">

        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->

    <div class="modal fade" id="measureModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content"></div>
        </div>
    </div> 

@endsection

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {

    var tblmaterial_measures = $('#tblmaterial_measures').dataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:  '{{ route('material_measures.list') }}',
                data: {
                    "material_id": $('#frmMeasure').data('id')
                }
            },
            columns: [
                        { data: 'DT_RowIndex', name: 'id' },
                        { data: 'material', name: 'material' },
                        { data: 'measure', name: 'measure' },
                        { data: 'unit', name: 'unit' },                        
                        { data: 'action', name: 'action' }
            ],            
            columnDefs: [{
                targets: [0],
                visible: true,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}            
        });

    $('#measureModal').on("show.bs.modal", function(e){
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    $(document).on("click","#add_measure", function(ev)  {

        if(ev.target != this) return false;
        var datastring = $("#frmMaterialMeasure").serializeArray();

        $.ajax({
            type: 'post',
            url: "{{ URL::route('material_measures.store') }}",
            data: datastring,
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(data) {
                if ((data.errors)){
                    var alertType = data.status_code;
                    var alertMessage = data.message;
                    var alerter = toastr[alertType];

                    if (alerter) {
                        $.each( data.errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });

                        alerter(alertMessage);
                    } else {
                        toastr.error("toastr alert-type " + alertType + " is unknown");
                    }
                }
                else {
                    tblmaterial_measures.DataTable().api().ajax.reload();
                    toastr.success(" {{ __('material_measures.success_created') }} ");
                    $('#measureModal').modal('hide');
                }
            },

        });
    });

    $(document).on("click","#edit_measure", function(ev)  {

        if(ev.target != this) return false;
        var cRows = $(ev.currentTarget).data('id');

        $.ajax({
            type: 'post',
            url: $("#frmMaterialMeasure").prop('action') ,
            data: $("#frmMaterialMeasure").serializeArray(),
            error: function (xhr, status, errorThrown) {
                var alertType = 'error';
                var alertMessage = xhr.status + ' - ' + errorThrown;
                var alerter = toastr[alertType];

                if (xhr.status == '422') {
                    var $errors = xhr.responseJSON;
                    $.each( $errors, function( key, value ) {
                        alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                    });
                    alerter(alertMessage);
                } else {
                    //Here the status code can be retrieved like;
                    alert(xhr.status + ' - ' + errorThrown );
                }

            },
            success: function(data) {
                if ((data.errors)){
                    var alertType = data.status_code;
                    var alertMessage = data.message;
                    var alerter = toastr[alertType];

                    if (alerter) {
                        $.each( data.errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });

                        alerter(alertMessage);
                    } else {
                        toastr.error("toastr alert-type " + alertType + " is unknown");
                    }
                }
                else {
                    tblmaterial_measures.DataTable().api().ajax.reload();             
                    toastr.success(" {{ __('material_measures.success_created') }} ");
                    $('#measureModal').modal('hide');
                }
            },

        });

    });    

});
</script>
@endsection