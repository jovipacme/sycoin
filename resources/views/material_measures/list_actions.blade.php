<a href="{{ route('material_measures.edit', ['id'=> $item->id] ) }}" class="btn btn-primary btn-xs edit_measure"
data-toggle='modal' data-remote='false' data-target = '#measureModal' data-mode='edit' >
    {{ __('material_measures.update') }}
</a> 
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['material_measures', $item->id ],
    'style' => 'display:inline'
]) !!}
    {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
{!! Form::close() !!}