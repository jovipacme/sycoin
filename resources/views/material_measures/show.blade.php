@extends('layouts.app')
@section('htmlheader_title')
{{ __('material_measures.singular') }}
@stop

@section('main-content')

    <h1>{{ __('material_measures.singular') }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>{{ __('material_measures.material') }}</th><th>{{ __('material_measures.measure') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $material_measure->id }} </td>
                    <td> {{ $material_measure->material->description }} </td>
                    <td> {{ $material_measure->measure->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection