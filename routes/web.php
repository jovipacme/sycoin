<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'PageController@about')->name('about');
Route::post('/about',  'PageController@about')->name('about');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => ['web']], function () {
        Route::get('business_partners/identity-document-autocomplete', 'Business_PartnersController@dataIdentityDocument')->name('business_partners.identity-document-autocomplete');
        Route::resource('business_partners', 'Business_PartnersController');

        Route::resource('business_partner_documents', 'Business_Partner_DocumentsController');
        Route::resource('material_providers', 'Material_ProvidersController');
        Route::resource('prices_list', 'Prices_ListController');        

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('stocks/list', 'StocksController@list')->name('stocks.list');
            Route::resource('stocks', 'StocksController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('material_measures/list', 'Material_MeasuresController@list')->name('material_measures.list');
            Route::resource('material_measures', 'Material_MeasuresController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('good_entry/list', 'Good_EntryController@list')->name('good_entry.list');
            Route::get('good_entry/add-provider', 'Good_EntryController@addProvider')->name('good_entry.addProvider');
            Route::get('good_entry/providers-autocomplete', 'Good_EntryController@dataProviders')->name('good_entry.providers-autocomplete');
            Route::get('good_entry/process', 'Good_EntryController@process')->name('good_entry.process');
            Route::resource('good_entry', 'Good_EntryController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('detail_good_entry/materials-autocomplete', 'Detail_Good_EntryController@dataMaterials')->name('detail_good_entry.materials-autocomplete');
            Route::resource('detail_good_entry', 'Detail_Good_EntryController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('delivery_order/list', 'Delivery_OrderController@list')->name('delivery_order.list');
            Route::get('delivery_order/add-customer', 'Delivery_OrderController@addCustomer')->name('delivery_order.addCustomer');
            Route::get('delivery_order/customers-autocomplete', 'Delivery_OrderController@dataCustomers')->name('delivery_order.customers-autocomplete');
            Route::get('delivery_order/process', 'Delivery_OrderController@process')->name('delivery_order.process');
            Route::resource('delivery_order', 'Delivery_OrderController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('detail_delivery_order/materials-autocomplete', 'Detail_Delivery_OrderController@dataMaterials')->name('detail_delivery_order.materials-autocomplete');
            Route::get('detail_delivery_order/material-measures-autocomplete', 'Detail_Delivery_OrderController@dataMeasures')->name('detail_delivery_order.material-measures-autocomplete');
            Route::resource('detail_delivery_order', 'Detail_Delivery_OrderController');
        });

        Route::group(['middleware' => ['caffeinated']], function () {
            Route::get('material/add-measure', 'MaterialController@addMeasure')->name('material.addMeasure');
            Route::resource('material', 'MaterialController');
        });

    });    

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
