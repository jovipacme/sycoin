
    <form id="frmMaterialMeasure" class="form-horizontal" role="form"
    action="{{ (isset($material_measure->id)) ? route('material_measures.update', $material_measure->id) : route('material_measures.store') }}"
    method="POST" enctype="multipart/form-data" autocomplete="off">
    @if(isset($material_measure->id))
        {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h5 class="modal-title"><b>{{ (isset($material_measure->id)) ? __('generic.edit') : __('generic.add_new') }} {{ __('material_measures.singular') }} </b></h5>
        </div>
        <div class="modal-body">
        @include('material_measures.fields')
        </div>
        <div class="modal-footer">
        @if(isset($material_measure->id))
        {!! Form::button( __('generic.edit'),['class' => 'btn btn-primary','id'=>'edit_measure','data-id'=>$material_measure->id]) !!}
        @else
        {!! Form::button( __('generic.new'),['class' => 'btn btn-primary','id'=>'add_measure']) !!}
        @endif

        {!! Form::button( __('generic.close'),['class' => 'btn btn-secondary','data-dismiss'=>'modal','id'=>'cancel_measure']) !!}
        </div>
    </form>