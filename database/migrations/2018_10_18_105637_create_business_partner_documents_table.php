<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessPartnerDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('business_partner_documents', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('partner_category_id')->unsigned();

                $table->timestamps();
                $table->softDeletes();
            });

            Schema::table('business_partner_documents', function(Blueprint $table) 
            {
                $table->foreign('partner_category_id')->references('id')->on('partner_categories');
            });             
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('business_partner_documents');
    }

}
