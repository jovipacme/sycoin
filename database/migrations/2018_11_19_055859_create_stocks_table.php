<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('stocks', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('material_id')->unsigned();
                $table->integer('measure_id')->unsigned();
                $table->bigInteger('stock')->default(0);
                $table->integer('min')->default(0);
                $table->integer('storehouse_id')->unsigned()->nullable();
                $table->boolean('locked')->default(false);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('material_id')->references('id')->on('materials');
                $table->foreign('measure_id')->references('id')->on('measures');
                $table->foreign('storehouse_id')->references('id')->on('storehouses');
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }

}
