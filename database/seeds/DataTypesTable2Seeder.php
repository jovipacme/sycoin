<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DataTypesTable2Seeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'measures');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'measures',
                'display_name_singular' => __('seeders.data_types.measure.singular'),
                'display_name_plural'   => __('seeders.data_types.measure.plural'),
                'icon'                  => 'voyager-compass',
                'model_name'            => 'App\\Measure',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => null,
            ])->save();
        }

        $dataType = $this->dataType('slug', 'material-categories');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'material_categories',
                'display_name_singular' => __('seeders.data_types.material_categories.singular'),
                'display_name_plural'   => __('seeders.data_types.material_categories.plural'),
                'icon'                  => 'voyager-categories',
                'model_name'            => 'App\\Material_Category',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => null,
            ])->save();
        }

        $dataType = $this->dataType('slug', 'materials');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'materials',
                'display_name_singular' => __('seeders.data_types.material.singular'),
                'display_name_plural'   => __('seeders.data_types.material.plural'),
                'icon'                  => 'voyager-tree',
                'model_name'            => 'App\\Material',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => null,
            ])->save();
        }

        $dataType = $this->dataType('slug', 'identity-documents');
        if (!$dataType->exists) {
            $dataType->insert([
                'name'                  => 'identity_documents',
                'slug'                  => 'identity-documents',
                'display_name_singular' => __('seeders.data_types.identity_documents.singular'),
                'display_name_plural'   => __('seeders.data_types.identity_documents.plural'),
                'icon'                  => 'voyager-credit-cards',
                'model_name'            => 'App\\Identity_Document',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => '{"order_column":null,"order_display_column":null}',
                'created_at'            =>  date("Y-m-d H:i:s"),
                'updated_at'            =>  date("Y-m-d H:i:s"),
            ]);
        }

        $dataType = $this->dataType('slug', 'partner-categories');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'partner_categories',
                'display_name_singular' => __('seeders.data_types.partner_categories.singular'),
                'display_name_plural'   => __('seeders.data_types.partner_categories.plural'),
                'icon'                  => 'voyager-categories',
                'model_name'            => 'App\\Partner_Category',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => null,
            ])->save();
        }

        $dataType = $this->dataType('slug', 'storehouses');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'storehouses',
                'display_name_singular' => __('seeders.data_types.storehouse.singular'),
                'display_name_plural'   => __('seeders.data_types.storehouse.plural'),
                'icon'                  => 'voyager-categories',
                'model_name'            => 'App\\Storehouse',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => null,
            ])->save();
        }        
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
