@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('business_partner.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('business_partner.singular') }}</h1>
    <hr/>

    {!! Form::model($business_partner, [
        'method' => 'PATCH',
        'url' => ['business_partners', $business_partner->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', __('business_partner.name'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
                {!! Form::label('telephone', __('business_partner.telephone'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('identity_document') ? 'has-error' : ''}}">
                {!! Form::label('identity_document_id', __('business_partner.identity_document_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('identity_document_id', $identityDocumentsList, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('identity_document_id', '<p class="help-block">:message</p>') !!}
                </div>
                {!! Form::label('identity_document', __('business_partner.identity_document'), ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('identity_document', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('identity_document', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', __('business_partner.contact'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', __('business_partner.email'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', __('business_partner.category_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('category_id', $categoriesList, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
                {!! Form::label('tagsList', __('business_partner.tags'), ['class' => 'col-sm-1 control-label']) !!}
                <div class="col-sm-3">
                    {!! Form::text('tagsList', null, ['class' => 'form-controls', 'data-role' => 'tagsinput',
                        'placeholder' => __('business_partner.placeholder_tags')]) !!}
                    {!! $errors->first('tagsList', '<p class="help-block">:message</p>') !!}
                </div>                
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('business_partner.update'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@section('scripts')
<script src="{{ asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tags').tagsinput({
            trimValue: true,
        });
      
    });   
</script>
@endsection