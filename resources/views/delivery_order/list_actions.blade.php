<a href="{{ route('delivery_order.edit', ['id'=> $item->id] ) }}" class="btn btn-primary btn-xs edit_delivery_order" >
    {{ __('generic.update') }}
</a> 
{!! Form::open([
    'method'=>'DELETE',
    'url' => ['delivery_order', $item->id ],
    'style' => 'display:inline'
]) !!}
    {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
{!! Form::close() !!}