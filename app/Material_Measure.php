<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


class Material_Measure extends BaseModel
{
  //use Traits\HasCompositePrimaryKey;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'material_measures';

    /**
     * The primary key of the table.
     *
     * @var array
     */
    //protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['material_id', 'measure_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function material() {
        return $this->belongsTo('App\Material');
    }

    public function measure() {
        return $this->belongsTo('App\Measure');
    }    
}
