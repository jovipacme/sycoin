<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Measure extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'measures';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'unit'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function materials()
    {
      return $this->belongsToMany('App\Material', 'material_measures');
    }
    
}
