@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.add_new') }} {{ __('business_partner.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.add_new') }} {{ __('business_partner.singular') }}</h1>
    <hr/>

    {!! Form::open(['url' => 'business_partners', 'class' => 'form-horizontal']) !!}
    
            <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
                {!! Form::label('category_id', __('business_partner.category_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('category_id', $categoriesList, null, ['class' => 'form-control']) !!}
                    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                </div>
                {!! Form::label('tags', __('business_partner.tags'), ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('tags', null, ['class' => 'form-controls', 'data-role' => 'tagsinput',
                        'placeholder' => __('business_partner.placeholder_tags')]) !!}
                    {!! $errors->first('tags', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', __('business_partner.name') , ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
                {!! Form::label('telephone',  __('business_partner.telephone'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('identity_document') ? 'has-error' : ''}}">
                {!! Form::label('identity_document_id', __('business_partner.identity_document_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::select('identity_document_id', $identityDocumentsList, null, ['placeholder' => __('form.type_selectdropdown'),'class' => 'form-control']) !!}
                    {!! $errors->first('identity_document_id', '<p class="help-block">:message</p>') !!}
                </div>            
                {!! Form::label('identity_document', __('business_partner.identity_document'), ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('identity_document', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('identity_document', '<p class="help-block">:message</p>') !!}
                </div>
            </div>            
            <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                {!! Form::label('contact', __('business_partner.contact'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', __('business_partner.email'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-8">
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('business_partner.create'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        <div class="col-sm-3">
            {{ link_to(PreviousRoute::getNamedRoute('session.index.back'), __('generic.cancel'), ['class' => 'btn btn-primary form-control']) }}
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@section('scripts')
<script src="{{ asset('/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tags').tagsinput({
            trimValue: true,
        });
        $('#tags').tagsinput('removeAll');

        $('#identity_document_id').select2({
        placeholder: "{{ __('form.type_selectdropdown') }}",
        allowClear:true,
        minimumInputLength: 2,
        theme: "bootstrap",
        ajax: {
            url: "{{ URL::route('business_partners.identity-document-autocomplete') }}",
            dataType: 'json',
            //delay: 250,
            processResults: function (data,page) {
                return {
                results:  $.map(data, function (item) {
                        return {
                            id: item.id,
                            text: item.partner_category + '\xa0' + item.identity_document,
                            name: item.partner_category,
                            description: item.identity_document
                        }
                    })
                };
            },
            cache: true
            },
        });

        $('#category_id').on("change", function(e){
            var _this = $(this).val();
            datastring = {material_id: _this };
            $.ajax({
                dataType: 'json',
                url: "{{ URL::route('business_partners.identity-document-autocomplete') }}",
                data: datastring,
                error: function (xhr, status, errorThrown) {
                    var alertType = 'error';
                    var alertMessage = xhr.status + ' - ' + errorThrown;
                    var alerter = toastr[alertType];

                    if (xhr.status == '422') {
                        var $errors = xhr.responseJSON;
                        $.each( $errors, function( key, value ) {
                            alertMessage += '<li>' + key + ':' + value + '</li>'; //showing only the first error.
                        });
                        alerter(alertMessage);
                    } else {
                        //Here the status code can be retrieved like;
                        alert(xhr.status + ' - ' + errorThrown );
                    }

                },
                success: function(items) {
                    if (items){
                        var newOptions = '<option value="">-- Select --</option>';
                        for(var idx in items) {
                            newOptions += '<option value="'+ items[idx].id +'">'+ items[idx].name +'</option>';
                        }
                        $('#identity_document_id').select2('destroy').html(newOptions)
                            .prop("disabled", false)
                            .select2({
                                placeholder: "{{ __('form.type_selectdropdown') }}",
                                allowClear:true,
                                theme: "bootstrap",
                            });
                    }
                    else {
                        toastr.error(" {{ __('delivery_order.error_updating') }} ");                    
                    }
                },

            });
        });

    });   
</script>
@endsection