<div class="table table-responsive">
    <table class="table table-bordered table-striped table-hover" id="tblmaterial_measures">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ __('material_measures.material') }}</th>
                <th>{{ __('material_measures.measure') }}</th>
                <th>{{ __('material_measures.unit') }}</th>
                <th>{{ __('material_measures.actions') }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($material_measures as $item)
            <tr data-id="{{ $item->id }}">
                <td><a href="{{ url('material_measures', $item->id) }}">{{ $loop->iteration }}</a></td>
                <td>{{ $item->material->name }}</td>
                <td>{{ $item->measure->name }}</td>
                <td>{{ $item->measure->unit }}</td>
                <td>
                    <?php $data = serialize(['id'=>$item->id,'material_id'=>$item->material_id,'measure_id'=> $item->measure_id]); ?>
                    <a href="{{ route('material_measures.edit', ['id'=> $item->id] ) }}" class="btn btn-primary btn-xs edit_measure"
                    data-toggle='modal' data-remote='false' data-target = '#measureModal' data-mode='edit' >
                        {{ __('material_measures.update') }}
                    </a> 
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['material_measures', $item->id ],
                        'style' => 'display:inline'
                    ]) !!}
                        {!! Form::submit( __('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>