<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   // Specified key was too long error
        Schema::defaultStringLength(191);
        //setlocale(LC_TIME, 'es_ES.UTF-8');
        \Carbon\Carbon::setLocale( config('app.locale') );
        \Carbon\Carbon::setUTF8(true);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Helpers/PreviousRoute.php';
    }
}
