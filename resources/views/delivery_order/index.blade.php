@extends('layouts.app')
@section('htmlheader_title')
{{ __('delivery_order.delivery_order') }}
@stop

@section('main-content')

    <h1>{{ __('delivery_order.delivery_order') }} <a href="{{ url('delivery_order/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add') }} {{ __('delivery_order.delivery_order') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tbldelivery_orders">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ __('delivery_order.customer_id') }}</th>
                    <th>{{ __('delivery_order.doc_serie') }}</th>
                    <th>{{ __('delivery_order.doc_num') }}</th>
                    <th>{{ __('delivery_order.doc_date') }}</th>
                    <th>{{ __('delivery_order.total') }}</th>
                    <th>{{ __('delivery_order.status_id') }}</th>                    
                    <th>{{ __('delivery_order.actions') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tbldelivery_orders').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('delivery_order.list') !!}",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'business_partner_id', name: 'business_partner_id' },
                { data: 'doc_serie', name: 'doc_serie' },
                { data: 'doc_num', name: 'doc_num' },
                { data: 'doc_date', name: 'doc_date' },
                { data: 'total', name: 'total' },
                { data: 'status_id', name: 'status_id' },
                { data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            order: [[5, "desc"]],            
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection