<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PartnerCategoriesTableSeeder::class);
        $this->call(IdentityDocumentsTableSeeder::class);
        $this->call(MenuItemsTable2Seeder::class);
        $this->call(DataTypesTable2Seeder::class);
        $this->call(DataRowsTable2Seeder::class);
        $this->call(PermissionsTable2Seeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(StorehousesTableSeeder::class);
    }
}
