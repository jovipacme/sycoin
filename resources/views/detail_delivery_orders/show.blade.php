@extends('layouts.app')

@section('main-content')
    <section class="content-header">
        <h1>
            Detail Delivery Order
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('detail_delivery_orders.show_fields')
                    <a href="{!! route('detail_delivery_order.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
