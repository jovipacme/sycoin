@extends('layouts.app')
@section('htmlheader_title')
{{ __('good_entry.good_entry') }}
@stop

@section('main-content')

    <h1>{{ __('good_entry.good_entry') }} <a href="{{ url('good_entry/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add') }} {{ __('good_entry.good_entry') }}</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblgoods_entry">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ __('good_entry.business_partner_id') }}</th>
                    <th>{{ __('good_entry.doc_serie') }}</th>
                    <th>{{ __('good_entry.doc_num') }}</th>
                    <th>{{ __('good_entry.doc_date') }}</th>
                    <th>{{ __('good_entry.total') }}</th>
                    <th>{{ __('good_entry.status_id') }}</th>
                    <th>{{ __('good_entry.actions') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblgoods_entry').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('good_entry.list') !!}',
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'business_partner_id', name: 'business_partner_id' },
                { data: 'doc_serie', name: 'doc_serie' },
                { data: 'doc_num', name: 'doc_num' },
                { data: 'doc_date', name: 'doc_date' },
                { data: 'total', name: 'total' },
                { data: 'status_id', name: 'status_id' },
                { data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            order: [[5, "desc"]],             
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection