<?php

namespace App;

use App\Models\BaseModel;
use App\Models\ModelCompositePrimaryKey;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Material extends BaseModel
{
    use SoftDeletes, SoftCascadeTrait, SearchableTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'materials';
    protected $softCascade = ['material_measures','material_providers','stocks'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name', 'description', 'material_category_id'];

    protected $dates = ['deleted_at'];

    /**
     * Columns and their priority in search results.
     * Columns with higher values are more important.
     * Columns with equal values have equal importance.
     *
     * @var array
     */
    protected $searchable  = [
        'columns' =>[
            'name' => 20,
            'description' => 10,
            'code' => 5,
        ]
    ];

    public function measures() {
        // you need to specify the pivot table, since eloquent would search for material_measure as default
        return $this->belongsToMany('App\Measure', 'material_measures');   
    }

    public function providers() {
        // you need to specify the pivot table, since eloquent would search for material_business_partner as default
        return $this->belongsToMany('App\Business_Partner', 'material_providers');   
    }

    public function material_measures() {
        return $this->hasMany('App\Material_Measure');
    } 

    public function material_category() {
        return $this->belongsTo('App\Material_Category');
    } 

    public function stocks() {
        return $this->hasMany('App\Stock');
    }

    public function material_providers() {
        return $this->hasMany('App\Material_Provider');
    }        
}
