<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use Conner\Tagging\Taggable;
class Business_Partner extends BaseModel
{   use SoftDeletes, SearchableTrait, Taggable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'business_partners';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'telephone', 'email', 'contact', 'category_id', 'identity_document_id', 'identity_document'];

    protected $dates = ['deleted_at'];

    /**
     * Columns and their priority in search results.
     * Columns with higher values are more important.
     * Columns with equal values have equal importance.
     *
     * @var array
     */
    protected $searchable  = [
        'columns' =>[
            'name' => 20,
            'identity_document' => 10,
            'contact' => 5,
        ]
    ];

    public function partner_category()
    {
        return $this->belongsTo('App\Partner_Category');
    }
}
