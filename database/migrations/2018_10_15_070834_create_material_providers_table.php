<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaterialProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('material_providers', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('material_id')->unsigned();
                $table->integer('business_partner_id')->unsigned();
                $table->integer('measure_id')->unsigned();
                $table->mediumInteger('stock')->default(0);
                $table->float('last_price')->default(0);
                $table->date('last_purchase')->nullable();
                $table->boolean('active')->default(true);
                //$table->unique(['material_id', 'business_partner_id','measure_id']);
                $table->timestamps();
                $table->softDeletes();

            });

            Schema::table('material_providers', function(Blueprint $table) 
            {
                $table->foreign('material_id')->references('id')->on('materials');
                $table->foreign('business_partner_id')->references('id')->on('business_partners');
                $table->foreign('measure_id')->references('id')->on('measures');
            });            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_providers');
    }

}
