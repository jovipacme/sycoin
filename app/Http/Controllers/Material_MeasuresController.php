<?php

namespace App\Http\Controllers;

use Session;
use App\Measure;

use App\Helpers\PreviousRoute;
use Carbon\Carbon;
use App\Http\Requests;
use App\Material_Measure;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialMeasureCreateRequest;
use App\Http\Requests\MaterialMeasureUpdateRequest;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class Material_MeasuresController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {   
        if ($request->exists('material_id')==true) {
            $material_measures = Material_Measure::where('material_id',$request->material_id)->get();
            PreviousRoute::setNamedRoute('session.index.back',['material_id' => $request->material_id]);
        } else {
            $material_measures = Material_Measure::all();
            PreviousRoute::setNamedRoute('session.index.back');
        }       
        return view('material_measures.index', compact('material_measures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        if ($request->exists('material_id')==true && empty($request->material_id)==false) {
            $materialsList = \App\Material::where('id',$request->material_id)->pluck('name', 'id');
        } else {
            $materialsList = \App\Material::pluck('description', 'id');
        }
       
        $measuresList = Measure::pluck('name', 'id');

        //Si es una peticion ajax
        if( $request->ajax() ){
            return view('material_measures.modal',compact('materialsList','measuresList'));
        } else {
            return view('material_measures.create',compact('materialsList','measuresList'));
        }        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MaterialMeasureCreateRequest $request)
    {
        
        $data = Material_Measure::create($request->all());
        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Material_Measure::select("material_measures.id","materials.description AS material","measures.name AS measure","measures.unit")
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
            ->where('material_measures.id',$data->id)->first();
            return response()->json($detail);
        } else {
            toast()->success( __('material_measures.error_created'), __('material_measures.message'));
            return redirect( PreviousRoute::getNamedRoute('session.index.back') );
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {   
        $material_measure = Material_Measure::findOrFail($id);

        return view('material_measures.show', compact('material_measure'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit(Request $request,$id)
    {
        //$params = unserialize($id);        
        $material_measure = Material_Measure::findOrFail($id);

        $materialsList = \App\Material::where('id',$material_measure['material_id'])->pluck('name', 'id');
        $measuresList = \App\Measure::pluck('name', 'id');

        //Si es una peticion ajax
        if( $request->ajax() ){
            return view('material_measures.modal',compact('material_measure','materialsList','measuresList'));
        } else {
            return view('material_measures.edit', compact('material_measure','materialsList','measuresList'));
        }        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, MaterialMeasureUpdateRequest $request)
    {

        $material_measure = Material_Measure::findOrFail($id);

        $data = $material_measure->update($request->all());

        //Si es una peticion ajax
        if( $request->ajax() ){
            $detail = Material_Measure::select("material_measures.id","materials.description AS material","measures.name AS measure","measures.unit")
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
            ->where('material_measures.id',$material_measure->id)->first();
            return response()->json($detail);
        } else {
            toast()->success( __('material_measures.error_updated'), __('material_measures.message'));
            return redirect( PreviousRoute::getNamedRoute('session.index.back', url()->previous() ));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material_measure = Material_Measure::findOrFail($id);

        $material_measure->delete();

        toast()->success( __('material_measures.error_deleted'), __('material_measures.message'));
        

        return redirect( PreviousRoute::getNamedRoute('session.index.back',url()->previous() ) );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function list(Request $request)
    {   
        if ($request->exists('material_id')==true && !empty($request->material_id)) {
            $material_measures = Material_Measure::select("material_measures.id","materials.name AS material","measures.name AS measure","measures.unit")
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')
            ->where('material_id',$request->material_id)->get();
        } else {
            $material_measures = Material_Measure::select("material_measures.id","materials.name AS material","measures.name AS measure","measures.unit")
            ->join('materials', 'materials.id', '=', 'material_measures.material_id')
            ->join('measures', 'measures.id', '=', 'material_measures.measure_id')->get();
        }
        if ($request->has('showDeleted') && $request->get('showDeleted') == 1) {
            $material_measures = $material_measures->withTrashed();
        }

        return Datatables::of($material_measures)
            ->setRowClass(function ($item) {
                return "material_measures" . ($item->trashed() ? ' danger' : '');
            })           
            ->setRowData([
                'data-id' => function($item) {
                    return $item->id;
                }
            ])
            ->addColumn('action', function($item) {
                return view('material_measures.list_actions', compact('item'))->render();
            })
            ->addIndexColumn()
            ->make(true);
            
    }

}
