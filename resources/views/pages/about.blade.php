@extends('layouts.app')

@section('htmlheader_title')
    {{ setting('site.title') }} Acerca de
@endsection
@section('main-content')
    <div class="container">
        <div class="content">
            <!-- Introduction Row -->
            <div class="row">
                <div class="col-lg-12">
                    <h1>Acerca de: SYCOIN </h1>
                    <h3>Estudiante:</h3>
                    <h4>Cynthia Rossana Queché Martínez</h4>
                    <h4>Carné: 1490102433</h4>
                    <h4>Año 2018</h4>
                    <h4>Facultad de Ingeniería en Sistemas y Ciencias de la Computación.</h4>
                    
                </div>
            </div>

            <!-- Team Members Row -->
            <div class="row">
                <div class="col-lg-12">
                </div>
                <div class="col-lg-12 text-center">
                    <img class="img-circle img-responsive img-center" src="//c1.staticflickr.com/2/1310/536290773_2ea31909bc_m.jpg" style="margin: 0 auto;">
                    <h3>Universidad Mariano Gálvez de Guatemala <h3>Sede Quetzaltenango.</h3>
                        <small></small>
                    </h3>
                    <h5>El Sistema Control de Inventario SYCOIN es una aplicación que permite llevar el control de 
                         <h5>ingresos y salidas de materiales que se utilizan en la Asociación Mercado Global
                        Guatemala.
                        </h5>
                    </h5>
                </div>
            </div>
        </div>
    </div>
@endsection 
