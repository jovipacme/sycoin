<?php

use Illuminate\Database\Seeder;
use App\Partner_Category;
use App\Identity_Document;

class IdentityDocumentsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $partnerCategories = Partner_Category::all();
        
        foreach($partnerCategories as $partnerCategory){
            $identityDocument = $this->dataRow($partnerCategory, __('seeders.identity_document.dpi'));
            if (!$identityDocument->exists) {
                $identityDocument->fill([
                        'name' => __('seeders.identity_document.dpi.name'),
                        'description' => __('seeders.identity_document.dpi.description')
                    ])->save();
            }
            $identityDocument = $this->dataRow($partnerCategory, __('seeders.identity_document.nit'));
            if (!$identityDocument->exists) {
                $identityDocument->fill([
                        'name' => __('seeders.identity_document.nit.name'),
                        'description' => __('seeders.identity_document.nit.description')
                    ])->save();
            }
            $identityDocument = $this->dataRow($partnerCategory, __('seeders.identity_document.cui'));
            if (!$identityDocument->exists) {
                $identityDocument->fill([
                        'name' => __('seeders.identity_document.cui.name'),
                        'description' => __('seeders.identity_document.cui.description')
                    ])->save();
            }            
        }

    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return Identity_Document::firstOrNew([
                'partner_category_id' => $type->id,
                'name' => $field,
            ]);
    }    
}
