@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.add_new') }} {{ __('business_partner_documents.document_singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.add_new') }} {{ __('business_partner_documents.document_singular') }}</h1>
    <hr/>

    {!! Form::open(['url' => 'business_partner_documents', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', __('business_partner_documents.name'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('partner_category_id') ? 'has-error' : ''}}">
                {!! Form::label('partner_category_id', __('business_partner_documents.business_partner_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('partner_category_id', $partnerCategoryList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('partner_category_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit(__('business_partner_documents.create'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection