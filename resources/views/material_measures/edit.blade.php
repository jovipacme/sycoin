@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('material_measures.singular') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('material_measures.singular') }}</h1>
    <hr/>

    {!! Form::model($material_measure, [
        'method' => 'PATCH',
        'url' => ['material_measures', $material_measure->id ],
        'class' => 'form-horizontal'
    ]) !!}

        @include('material_measures.fields')

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('material_measures.update'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
        <div class="col-sm-3">
            {{ link_to(PreviousRoute::getNamedRoute('session.index.back'), __('generic.cancel'), ['class' => 'btn btn-primary form-control']) }}
        </div>        
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection