<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTable2Seeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        /**
         * BackEnd Menus
         */

        $menuAdmin = Menu::where('name', 'admin')->firstOrFail();

        $materialMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.commodities'),
            'url'     => '',
        ]);
        if (!$materialMenuItem->exists) {
            $materialMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-tree',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 15,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.categories'),
            'url'     => '',
            'route'   => 'voyager.material-categories.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-categories',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 16,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.measure'),
            'url'     => '',
            'route'   => 'voyager.measures.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-watch',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 17,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.materials'),
            'url'     => '',
            'route'   => 'voyager.materials.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-leaf',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 18,
            ])->save();
        }

        $businessPartnerMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.business_partner'),
            'url'     => '',
        ]);
        if (!$businessPartnerMenuItem->exists) {
            $businessPartnerMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-people',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 19,
            ])->save();
        }
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.identity_documents'),
            'url'     => '',
            'route'   => 'voyager.identity-documents.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-credit-cards',
                'color'      => null,
                'parent_id'  => $businessPartnerMenuItem->id,
                'order'      => 20,
            ])->save();
        };
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.partner_categories'),
            'url'     => '',
            'route'   => 'voyager.partner-categories.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-categories',
                'color'      => null,
                'parent_id'  => $businessPartnerMenuItem->id,
                'order'      => 20,
            ])->save();
        };       

        $stockMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.stock'),
            'url'     => '',
        ]);
        if (!$stockMenuItem->exists) {
            $stockMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-home',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 22,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuAdmin->id,
            'title'   => __('seeders.menu_items.storehouses'),
            'url'     => '',
            'route'   => 'voyager.storehouses.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-treasure',
                'color'      => null,
                'parent_id'  => $stockMenuItem->id,
                'order'      => 23,
            ])->save();
        }

        /**
         * FrontEnd Menus
         */
        $menuFrontend = Menu::where('name', 'frontend')->firstOrFail();

        $materialMenuItem = MenuItem::firstOrNew([
            'menu_id' => $menuFrontend->id,
            'title'   => __('seeders.menu_items.commodities'),
            'url'     => '',
        ]);
        if (!$materialMenuItem->exists) {
            $materialMenuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-tree',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 2,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuFrontend->id,
            'title'   => __('seeders.menu_items.material'),
            'url'     => '',
            'route'   => 'material.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-leaf',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 3,
            ])->save();
        }

        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menuFrontend->id,
            'title'   => __('seeders.menu_items.measure'),
            'url'     => '',
            'route'   => 'material_measures.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-watch',
                'color'      => null,
                'parent_id'  => $materialMenuItem->id,
                'order'      => 4,
            ])->save();
        }        
    }
}
