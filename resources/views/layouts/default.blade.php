<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ app()->getLocale() }}">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<style>
  body {

  }

  .container {
    text-align: center;
    display: table-cell;
    vertical-align: middle;
  }

  .content {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    font-weight: 100;   
    text-align: center;
    display: inline-block;
  }

  .title {
    font-size: 156px;
  }

  .quote {
    font-size: 36px;
  }

  .explanation {
    font-size: 24px;
  }
</style>

<body class="skin-green layout-top-nav">
<div id="app" v-cloak>
    <div class="wrapper">

    @include('layouts.partials.mainheader')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container">
        @include('layouts.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
        </div>
    </div><!-- /.content-wrapper -->

    @include('layouts.partials.controlsidebar')

    @include('layouts.partials.footer')

</div><!-- ./wrapper -->
</div>
@section('global-scripts')
    <!-- Scripts -->
    @include('layouts.partials.scripts')
@show
@yield('scripts')
</body>
</html>
