@extends('voyager::master')

@section('page_title', __('generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ (isset($dataTypeContent->id)) ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
            @endif
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                    {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
                                {!! Form::label('material_id', __('generic.username'), ['class' => 'control-label']) !!}
                                    {!! Form::select('material_id', $materialsList,"@if(isset($dataTypeContent->material_id)){{ $dataTypeContent->material_id }}@endif",
                                        ['class' => 'form-control', 'required' => 'required']) !!}    
                                    {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
                                {!! Form::label('measure_id', 'Measure Id: ', ['class' => 'control-label']) !!}
                                    {!! Form::select('measure_id', $measuresList,"@if(isset($dataTypeContent->measure_id)){{ $dataTypeContent->measure_id }}@endif",
                                        ['class' => 'form-control', 'required' => 'required']) !!}
                                    {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('generic.save') }}
            </button>
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
