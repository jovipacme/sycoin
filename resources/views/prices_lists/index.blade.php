@extends('layouts.app')
@section('htmlheader_title')
Pricelist
@stop

@section('main-content')

    <h1>Pricelists <a href="{{ url('prices_list/create') }}" class="btn btn-primary pull-right btn-sm">Add New Pricelist</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblpricelists">
            <thead>
                <tr>
                    <th>ID</th><th>Name</th><th>User Id</th><th>Percent Profit</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($prices_list as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('prices_list', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->user_id }}</td><td>{{ $item->percent_profit }}</td>
                    <td>
                        <a href="{{ url('prices_list/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['prices_list', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblpricelists').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection