<?php

namespace App\Http\Controllers;

use Session;
use App\Measure;

use App\Material;
use Carbon\Carbon;
use App\Http\Requests;
use App\Material_Category;
use App\Material_Measure;
use App\Helpers\PreviousRoute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MaterialController extends Controller
{

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'name' => 'required',
        'description' => 'nullable',        
        'material_category_id' => 'required',
        'code'=>'nullable|alpha_dash'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $material = Material::all();

        PreviousRoute::setNamedRoute('session.index.back');
        return view('material.index', compact('material'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        PreviousRoute::setNamedRoute('session.previous_route');
        $categoriesList = Material_Category::pluck('name', 'id');
        
        return view('material.create', compact('categoriesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }
        
        $material = Material::create($request->all());

        if($request->has('measures')){
            $material->measures()->attach($request->input('measures'));
        }

        if($request->has('providers')){
            $material->providers()->attach($request->input('providers'));
        }

        toast()->success( __('material.error_created'), __('material.message') );

        return redirect('material');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $material = Material::findOrFail($id);

        return view('material.show', compact('material'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // main form
        $material = Material::findOrFail($id);
        // Sub form Material Measure
        $categoriesList = Material_Category::pluck('name', 'id');
        $material_measures = Material_Measure::where('material_id',$id)->get();

        $measuresList = Measure::pluck('name', 'id');
        PreviousRoute::setNamedRoute('session.index.back',$id);
        return view('material.edit', compact('material','categoriesList','material_measures','measuresList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $validation = validator()->make($request->all(),$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            $validation->validate();
        }

        $material = Material::findOrFail($id);
        $material->update($request->all());

        toast()->success( __('material.error_updated'), __('material.message'));
        

        return redirect('material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $material = Material::findOrFail($id);

        $material->delete();

        toast()->success( __('material.error_deleted'), __('material.message') );
        
        return redirect('material');
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function addMeasure(Request $request)
    {
        $measuresList = \App\Measure::pluck('name', 'id');
        if($request->has('material_id')){
            $material_id = $request->material_id;
            $materialsList = \App\Material::where('id',$request->material_id)->pluck('name', 'id');
        }else {
            $material_id = '';
            $materialsList = \App\Material::pluck('name', 'id');
        }

        return view('material_measures.modal', compact('material_id','materialsList','measuresList'));       
    }      

}
