@extends('layouts.app')
@section('htmlheader_title')
{{ __('material.plural') }}
@stop

@section('main-content')

    <h1>{{ __('material.plural') }}</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th>
                    <th>{{ __('material.code') }}</th>
                    <th>{{ __('material.name') }}</th>
                    <th>{{ __('material.description') }}</th>
                    <th>{{ __('material.category_material_id') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $material->id }}</td>
                    <td> {{ $material->code }} </td>
                    <td> {{ $material->name }} </td>
                    <td> {{ $material->description }} </td>
                    <td> {{ $material->material_category_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection