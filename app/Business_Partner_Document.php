<?php

namespace App;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business_Partner_Document extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'business_partner_documents';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'partner_category_id'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function partner_category()
    {
        return $this->belongsTo('App\Partner_Category');
    }    
}
