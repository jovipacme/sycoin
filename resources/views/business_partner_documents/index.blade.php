@extends('layouts.app')
@section('htmlheader_title')
{{ __('business_partner_documents.documents_plural') }}
@stop

@section('main-content')

    <h1>  {{ __('business_partner_documents.documents_plural') }} <a href="{{ url('business_partner_documents/create') }}" class="btn btn-primary pull-right btn-sm">{{ __('generic.add_new') }} </a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblbusiness_partner_documents">
            <thead>
                <tr>
                    <th>ID</th><th>{{ __('business_partner_documents.name') }}</th><th>{{ __('business_partner_documents.category_business_partner_id') }}</th><th>{{ __('business_partner_documents.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($business_partner_documents as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('business_partner_documents', $item->id) }}">{{ $item->name }}</a></td>
                    <td>
                        @if ($item->partner_category_id)
                            {{ $item->partner_category->name }}
                        @else
                            {{ $item->partner_category_id }}
                        @endif
                    </td>
                    <td>
                        <a href="{{ url('business_partner_documents/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs"> {{ __('generic.update') }}</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['business_partner_documents', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit(__('generic.delete'), ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblbusiness_partner_documents').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
            language: {!! json_encode(__('datatable')) !!}
        });
    });
</script>
@endsection