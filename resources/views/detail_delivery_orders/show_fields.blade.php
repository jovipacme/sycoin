<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
    <p class="form-control-static" >{!! $detailDeliveryOrder->id !!}</p>
    </div>
</div>

<!-- Delivery Order Id Field -->
<div class="form-group">
    {!! Form::label('delivery_order_id', 'Delivery Order Id:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
    <p class="form-control-static" >{!! $detailDeliveryOrder->delivery_order_id !!}</p>
    </div>
</div>

<!-- Material Field -->
<div class="form-group">
    {!! Form::label('material', 'Material:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->material->description !!}</p>
    </div>
</div>

<!-- Measure Id Field -->
<div class="form-group">
    {!! Form::label('measure_id', 'Measure Id:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->measure->name !!}</p>
    </div>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->quantity !!}</p>
    </div>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->price !!}</p>
    </div>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->subtotal !!}</p>
    </div>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->created_at !!}</p>
    </div>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        <p class="form-control-static" >{!! $detailDeliveryOrder->updated_at !!}</p>
    </div>
</div>

