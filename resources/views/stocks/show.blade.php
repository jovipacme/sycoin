@extends('layouts.app')
@section('htmlheader_title')
Stock
@stop

@section('main-content')

    <h1>Stock</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th>
                    <th>Material</th>
                    <th>Measure</th>
                    <th>Stock</th>
                    <th>Locked</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $stock->id }}</td>
                    <td>
                        @if ($stock->measure_id)
                            {{ $stock->measure->name }}
                        @else
                            {{ $stock->measure_id }}
                        @endif                    
                    </td>
                    <td>
                        @if ($stock->storehouse_id)
                            {{ $stock->storehouse->name }}
                        @else
                            {{ $stock->storehouse_id }}
                        @endif
                    </td>                  
                    <td> {{ $stock->stock }} </td>
                    <td> {{ $stock->locked }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection