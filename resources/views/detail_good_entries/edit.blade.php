<div class="box-body">
    <div class="col-lg-12">
        {!! Form::model($detailGoodEntry, ['route' => ['detail_good_entry.update', $detailGoodEntry->id], 'method' => 'patch',
            'class' => 'form-horizontal','id'=>'frmDetailGoodEntry','autocomplete' => 'off']) !!}
        <div class="row">   
            @include('detail_good_entries.fields')
        </div>
        {!! Form::close() !!}
    </div> <!-- col-12 !-->
</div> <!-- box body !-->