@extends('layouts.app')
@section('htmlheader_title')
{{ __('generic.edit') }} {{ __('material_providers.plural') }}
@stop

@section('main-content')

    <h1>{{ __('generic.edit') }} {{ __('material_providers.plural') }}</h1>
    <hr/>

    {!! Form::model($material_provider, [
        'method' => 'PATCH',
        'url' => ['material_providers', $material_provider->id],
        'class' => 'form-horizontal'
    ]) !!}

                <div class="form-group {{ $errors->has('material_id') ? 'has-error' : ''}}">
                {!! Form::label('material_id', __('material_providers.material_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('material_id',$materialList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('material_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('business_partner_id') ? 'has-error' : ''}}">
                {!! Form::label('business_partner_id', __('material_providers.provider_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('business_partner_id',$providerList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('business_partner_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('measure_id') ? 'has-error' : ''}}">
                {!! Form::label('measure_id', __('material_providers.measure_id'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::select('measure_id',$measureList, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('measure_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>            
            <div class="form-group {{ $errors->has('last_purchase') ? 'has-error' : ''}}">
                {!! Form::label('last_purchase', __('material_providers.last_purchase'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::date('last_purchase', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('last_purchase', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
                {!! Form::label('active', __('material_providers.active'), ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                                <div class="checkbox">
                <label>{!! Form::radio('active', '1') !!} Yes</label>
            </div>
            <div class="checkbox">
                <label>{!! Form::radio('active', '0', true) !!} No</label>
            </div>
                    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit( __('generic.update'), ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection