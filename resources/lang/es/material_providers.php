<?php

return [
    'plural'                => 'Proveedor de materiales',
    'create'                => 'Crear',
    'actions'               => 'Accion',
    'update'                => 'Actualizar',
    'material_id'           => 'ID Material',
    'provider_id'           => 'ID Proveedor',
    'measure_id'            => 'ID Medida',
    'last_purchase'         => 'Fecha de compra',
    'active'                => 'Activo',    
    'stock'                 => 'Stock',    
    'message'               => 'Mensaje',
    'error_creating'        => 'Lo siento, parece que hubo un problema al crear',
    'error_removing'        => 'Lo siento, parece que hubo un problema al eliminar',
    'error_updating'        => 'Lo siento, parece que hubo un problema al actualizar',
    'error_created'         => 'Proveedor de material creado exitosamente',
    'error_deleted'         => 'Proveedor  de material borrado exitosamente',
    'error_updated'         => 'Proveedor  de material se actualizo correctamente',
   ];
