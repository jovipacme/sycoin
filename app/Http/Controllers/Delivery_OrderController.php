<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\PreviousRoute;
use App\Delivery_Order;
use App\Detail_Delivery_Order;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use DB;

class Delivery_OrderController extends Controller
{
    private $business_partner;

    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */    
    protected $validationRules = [
        'business_partner_id' => 'required',
        'doc_date' => 'required',
        'partner_document_id' => 'required',
        'user_id' => 'required',
        'status_id' => 'required',
        'total'=>'nullable|numeric'
    ];

    public function __construct()
    {   //Assign constant for filter only Customers
        $this->business_partner = config('constants.BusinessPartner_Category.member');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $delivery_order = Delivery_Order::all();

        PreviousRoute::setNamedRoute('session.index.back');
        return view('delivery_order.index', compact('delivery_order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        // Check permission
        // $this->authorize('add', app('App\Delivery_Order'));

        if ($request->exists('id')==true && empty($request->id)==false) {
            $delivery_order = Delivery_Order::findOrFail($request->id);

            if ( $delivery_order->status_id == config('constants.DocumentStatus.closed') ) {
                toast()->warning( __('delivery_order.error_creating'), __('material.message'));
                $indexRoute = PreviousRoute::getNamedRoute('session.index.back');
                return redirect($indexRoute);
            }
        } else {
            $delivery_order = array();
        }

        $businessPartnerDocumentList = \App\Business_Partner_Document::select("id","name")
        ->where('partner_category_id','=',$this->business_partner)
        ->pluck('name', 'id');

        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');

        if ($request->exists('id')==true && empty($request->id)==false) {
            $customerList = \App\Business_Partner::select("id","name")->where('id',$delivery_order->business_partner_id)->pluck('name', 'id');

            $detailDeliveryOrder = Detail_Delivery_Order::select("detail_delivery_orders.id","detail_delivery_orders.delivery_order_id",
            "materials.name AS material","measures.name AS measure","detail_delivery_orders.quantity","detail_delivery_orders.price",
            "detail_delivery_orders.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_delivery_orders.material_id')
            ->join('measures', 'measures.id', '=', 'detail_delivery_orders.measure_id')
            ->where('detail_delivery_orders.delivery_order_id','=',$request->id)->get();

            PreviousRoute::setNamedRoute('session.action.back',['id' => $request->id]);

        } else {
            $detailDeliveryOrder = Detail_Delivery_Order::where('delivery_order_id',null)->get();
            $customerList = array();

            PreviousRoute::setNamedRoute('session.action.back');            
        }

        $statusList = \App\Document_Status::all();       
        $materialList = array();

        return view('delivery_order.edit-add',compact('delivery_order','detailDeliveryOrder','statusList','customerList','businessPartnerDocumentList','materialList','measureList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth()->user()->id;
        $inputs['status_id'] = 1; //abierto
 
        $validation = validator()->make($inputs,$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            //toast()->clear();
            $validation->validate();
        }

        $resource = Delivery_Order::create($inputs);

        toast()->success( __('delivery_order.success_created'), __('material.message'));

        return redirect()->route('delivery_order.create', array('id' => $resource->id) );
       //return redirect('delivery_order');
 
    }

    public function process(Request $request)
    {
        
        if($request->has('id')) {
            DB::beginTransaction();
            try {
                $delivery_order = Delivery_Order::findOrFail($request->id);
                $detailDeliveryOrder = Detail_Delivery_Order::where('delivery_order_id',$request->id)->get();
                
                    foreach($detailDeliveryOrder as $key=>$detail) {

                        $stock = \App\Stock::where([
                            ['material_id', '=', $detail->material_id],
                            ['measure_id', '=', $detail->measure_id]
                        ])->first();
                    
                        if ($stock) {
                            $stock->decrement('stock', $detail->quantity);
                            $stock->save();
                        } else {
                            $stock = \App\Stock::firstOrNew(
                                ['material_id' => $detail->material_id,
                                'measure_id' => $detail->measure_id,
                                'stock' => 0]
                            );
                            $stock->stock = $stock->stock - $detail->quantity;
                            $stock->save();
                        }
                    }

                    $inputs = $request->all();
                    $inputs['user_id'] = auth()->user()->id;
                    $inputs['status_id'] = config('constants.DocumentStatus.closed');
    
                    $delivery_order->update($inputs);

                    DB::commit();
                    $success = true;
                    
                } catch (\Exception $e) {
                    $success = false;
                    DB::rollback();
                }
                    if ($success) {
                        //Si es una peticion ajax
                        if( $request->ajax() ){
                            $indexRoute = PreviousRoute::getNamedRoute('session.index.back','delivery_order.index');
                            return response()->json(['success'=>true,'result'=>$request->input('id'),'url'=> $indexRoute]);
                        } else {
                            toast()->success( __('delivery_order.success_processed'), __('material.message'));
                            $indexRoute = PreviousRoute::getNamedRoute('session.index.back','delivery_order.index');
                            return redirect($indexRoute);
                        }  
                    } else {
                        //Si es una peticion ajax
                        if( $request->ajax() ){
                            $actionRoute = PreviousRoute::getNamedRoute('delivery_order.action.back');
                            return response()->json(['success'=>false,'result'=>$request->input('id'),'url'=> $indexRoute]);
                        } else {
                            toast()->error( __('delivery_order.error_processed'), __('material.message'));
                            $actionRoute = PreviousRoute::getNamedRoute('delivery_order.action.back');
                            return redirect($actionRoute);   
                        }
                    }
        } else {
            toast()->error( __('delivery_order.error_updating'), __('material.message'));
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $delivery_order = Delivery_Order::findOrFail($id);

        return view('delivery_order.show', compact('delivery_order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        PreviousRoute::setNamedRoute('session.action.back',$id);

        $delivery_order = Delivery_Order::findOrFail($id);

        if (empty($delivery_order->id)==false) {
            $customerList = \App\Business_Partner::select("id","name")->where('id',$delivery_order->business_partner_id)->pluck('name', 'id');

            $detailDeliveryOrder = Detail_Delivery_Order::select("detail_delivery_orders.id","detail_delivery_orders.delivery_order_id",
            "materials.name AS material","measures.name AS measure","detail_delivery_orders.quantity","detail_delivery_orders.price",
            "detail_delivery_orders.subtotal")
            ->join('materials', 'materials.id', '=', 'detail_delivery_orders.material_id')
            ->join('measures', 'measures.id', '=', 'detail_delivery_orders.measure_id')
            ->where('detail_delivery_orders.delivery_order_id','=',$delivery_order->id)->get();
            
        } else {
            $detailDeliveryOrder = Detail_Delivery_Order::where('delivery_order_id',null)->get();
            $customerList = array();
        }

        $businessPartnerDocumentList = \App\Business_Partner_Document::select("id","name")
        ->where('partner_category_id','=',$this->business_partner)
        ->pluck('name', 'id');

        $measureList = \App\Measure::select("id","name")->pluck('name', 'id');
        
        $materialList = array();
        $statusList = \App\Document_Status::all();

        return view('delivery_order.edit-add', compact('delivery_order','detailDeliveryOrder','customerList','businessPartnerDocumentList','materialList','measureList','statusList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = auth()->user()->id;
        $inputs['status_id'] = config('constants.DocumentStatus.pending');

        $validation = validator()->make($inputs,$this->validationRules);

        if ( $validation->fails() ) {
            $errors = $validation->errors();
            foreach ($errors->all() as $message) {
                toast()->error($message, __('json.validation_errors') );
            }
            //toast()->clear();
            $validation->validate();
        }

        $delivery_order = Delivery_Order::findOrFail($id);
        $delivery_order->update($inputs);

        toast()->success( __('delivery_order.success_updated'), __('material.message'));

        return redirect('delivery_order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $delivery_order = Delivery_Order::findOrFail($id);

        $delivery_order->delete();

        toast()->success( __('delivery_order.success_deleted'), __('material.message'));

        return redirect('delivery_order');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        if($request->has('trashed')) {
            $delivery_order = Delivery_Order::onlyTrashed()->get();
        } else {
            $delivery_order = Delivery_Order::get();
        } 

        return Datatables::of($delivery_order)
            ->setRowClass(function ($item) {
                return "delivery_order";
            })
            ->setRowData([
                'data-id' => function($item) {
                    return $item->id;
                }
            ])
            ->editColumn('business_partner_id', function($item) {
                return $item->business_partner->name;
            })              
            ->editColumn('doc_date', function($item) {
                return $item->doc_date->toFormattedDateString();
            })            
            ->editColumn('status_id', function($item) {
                return $item->status_description($item->status_id);
            })
            ->addColumn('action', function($item) {
                return view('delivery_order.list_actions', compact('item'))->render();
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
     public function dataCustomers(Request $request)
     {
         $data = [];
 
         if($request->has('q')){
             $search = $request->q;
        /*
             $data = DB::table("business_partners")
                     ->select("id","name")
                     ->where('category_id','=',$this->business_partner)
                     ->where('name','LIKE',"%$search%")
                     ->orwhere('identity_document','LIKE',"%$search%")
                     ->get();
        */
            $data = \App\Business_Partner::where('category_id','=',$this->business_partner)
                ->search($search)->get();
         }
         return response()->json($data);
     }    

    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
     public function addCustomer(Request $request)
     {
        $categoriesList = \App\Partner_Category::where('id','=',$this->business_partner)->pluck('name', 'id');

        return view('delivery_order.create_business_partner', compact('categoriesList'));       
     }     
}
